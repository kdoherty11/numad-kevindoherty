package com.kdoherty.cliche.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.repackaged.com.google.api.client.util.Strings;
import com.googlecode.objectify.Key;

import org.json.simple.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.kdoherty.gcm.endpoint.OfyService.ofy;

/**
 * Created by kdoherty on 11/13/14.
 */
@Api(name = "cliche", version = "v1.1",
        namespace = @ApiNamespace(ownerDomain = "endpoint.cliche.kdoherty.com",
                ownerName = "endpoint.cliche.kdoherty.com", packagePath=""))
public class ClicheEndpoint {

    private static final int MAX_ENTRIES = 25;

    private static final Logger log = Logger.getLogger(ClicheEndpoint.class.getName());

    static {
        log.setLevel(Level.INFO);
    }

    final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    /**
     * Most recent is better
     */
    private static enum ClicheDateComparator implements Comparator<ClicheEntity> {

        INSTANCE;

        @Override
        public int compare(ClicheEntity o1, ClicheEntity o2) {

            Date date1 = o1.getTimeStamp();
            Date date2 = o2.getTimeStamp();

            return date2.compareTo(date1);
        }
    }

    @ApiMethod(name = "getCliches", httpMethod = ApiMethod.HttpMethod.GET)
    public List<ClicheEntity> getCliches() {

        log.info("Get images called");

        List<ClicheEntity> cliches = ofy().load().type(ClicheEntity.class).limit(MAX_ENTRIES).list();

        Collections.sort(cliches, ClicheDateComparator.INSTANCE);

        log.info("Successfully got " + cliches.size() +  " cliches");

        return cliches;
    }

    @ApiMethod(name = "getScore", httpMethod = ApiMethod.HttpMethod.GET)
    public JSONObject getScore(@Named("userId") String id) {

        log.info("getScore called");

        List<ClicheEntity> cliches = getClichesByUserId(id);

        log.info("getScore has list of " + cliches.size() + " cliche submitted by the user");

        int totalScore = 0;

        for (ClicheEntity entity : cliches) {
            // Get points based on the number of votes received
            totalScore += entity.getScore();

            log.info("totalScore increased by " + entity.getScore());

            if (!Strings.isNullOrEmpty(entity.getExtraPictureBlobKey())) {
                log.info("totalScore increased by 5 because user submitted a bonus photo");
                totalScore += 5;
            }
        }

        JSONObject json = new JSONObject();
        json.put("score", totalScore);

        log.info("putting " + totalScore + " into json");

        return json;
    }

    @ApiMethod(name = "getClichesByUserId", httpMethod = ApiMethod.HttpMethod.GET)
    public List<ClicheEntity> getClichesByUserId(@Named("userId") String id) {

        log.info("Get images by id called");

        List<ClicheEntity> cliches =
                ofy().load().type(ClicheEntity.class).filter("userId", id).limit(MAX_ENTRIES).list();

        Collections.sort(cliches, ClicheDateComparator.INSTANCE);

        log.info("Successfully got images by id");

        return cliches;
    }

    @ApiMethod(name = "clearCliches")
    public void clearCliches() {
        List<Key<ClicheEntity>> keys = ofy().load().type(ClicheEntity.class).keys().list();
        ofy().delete().keys(keys);
        log.info("Successfully cleared images");
    }

    @ApiMethod(name = "getUploadUrl", httpMethod = ApiMethod.HttpMethod.GET)
    public JSONObject getUploadUrl() {

        log.info("Upload URL called");

        String uploadUrl = blobstoreService.createUploadUrl("/upload");

        JSONObject json = new JSONObject();
        json.put("uploadUrl", uploadUrl);

        log.info("Returning upload url of " + uploadUrl);

        return json;
    }

    @ApiMethod(name="upVoteByClicheId", httpMethod = ApiMethod.HttpMethod.POST)
    public void upVoteByClicheId(@Named("clicheId") long clicheId, @Named("userId") String userId) {
        ClicheEntity cliche = findClicheById(clicheId);
        if (!cliche.getVotedOn().contains(userId)) {
            cliche.addToVotedOn(userId);
            cliche.upVote();
            ofy().save().entity(cliche);
        }
    }

    @ApiMethod(name="downVoteByClicheId", httpMethod = ApiMethod.HttpMethod.POST)
    public void downVoteByClicheId(@Named("clicheId") long clicheId, @Named("userId") String userId) {
        ClicheEntity cliche = findClicheById(clicheId);
        if (!cliche.getVotedOn().contains(userId)) {
            cliche.addToVotedOn(userId);
            cliche.downVote();
            ofy().save().entity(cliche);
        }
    }

    @ApiMethod(name = "storeCliche", httpMethod = ApiMethod.HttpMethod.POST)
    public void storeCliche(ClicheEntity cliche) {

        log.info("Store image called");

        ofy().save().entity(cliche);

        log.info("Successfully stored ImageEntity");
    }

    @ApiMethod(name = "removeCliche", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void removeCliche(@Named("clicheId") long clicheId) {
        ClicheEntity entity = findClicheById(clicheId);
        ofy().delete().entity(entity);
    }

    private ClicheEntity findClicheById(long clicheId) {
        return ofy().load().type(ClicheEntity.class).filter("id", clicheId).first().now();
    }
}
