package com.kdoherty.cliche.endpoint;

/**
 * Created by kdoherty on 11/23/14.
 */

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Upload extends HttpServlet {

    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    private static final Logger log = Logger.getLogger(Upload.class.getName());

    static {
        log.setLevel(Level.INFO);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        log.info("Received post");

        List<BlobKey> blobs = blobstoreService.getUploads(req).get("file");

        BlobKey blobKey = blobs.get(0);

        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(blobKey);

        String servingUrl = imagesService.getServingUrl(servingOptions);

        res.setStatus(HttpServletResponse.SC_OK);
        res.setContentType("application/json");

        JSONObject json = new JSONObject();
        json.put("servingUrl", servingUrl);
        json.put("blobKey", blobKey.getKeyString());

        PrintWriter out = res.getWriter();
        out.print(json.toString());
        out.flush();
        out.close();

        log.info("Successfully uploaded blob " + blobKey);
    }
}