package com.kdoherty.cliche.endpoint;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kdoherty on 11/14/14.
 */
@Entity
public class ClicheEntity {

    @Id
    Long id;
    String servingUrl;
    String blobKey;
    @Index
    String userId;
    int score = 0;
    String cliche = "";
    String comment = "";
    String extraPictureServingUrl = "";
    String extraPictureBlobKey = "";

    final Date timeStamp = new Date();

    List<String> votedOn = new ArrayList<>();

    public ClicheEntity() {
        // Do not remove
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setServingUrl(String servingUrl) {
        this.servingUrl = servingUrl;
    }

    public String getServingUrl() {
        return servingUrl;
    }

    public void setBlobKey(String blobKey) {
        this.blobKey = blobKey;
    }

    public String getBlobKey() {
        return blobKey;
    }

    public String getCliche() {
       return cliche;
    }

    public void setCliche(String cliche) {
        this.cliche = cliche;
    }

    public void upVote() {
        score++;
    }

    public void downVote() {
        score--;
    }

    public long getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getExtraPictureServingUrl() {
        return extraPictureServingUrl;
    }

    public void setExtraPictureServingUrl(String extraPictureServingUrl) {
        this.extraPictureServingUrl = extraPictureServingUrl;
    }

    public String getExtraPictureBlobKey() {
        return extraPictureBlobKey;
    }

    public void setExtraPictureBlobKey(String extraPictureBlobKey) {
        this.extraPictureBlobKey = extraPictureBlobKey;
    }

    public void addToVotedOn(String id) {
        if (!votedOn.contains(id)) {
            votedOn.add(id);
        }
    }

    public List<String> getVotedOn() {
        return votedOn;
    }

    public void setVotedOn(List<String> votedOn) {
        this.votedOn = votedOn;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }
}
