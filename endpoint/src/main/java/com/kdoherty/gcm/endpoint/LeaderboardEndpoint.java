package com.kdoherty.gcm.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.kdoherty.gcm.endpoint.OfyService.ofy;

/**
 * Created by kdoherty on 10/29/14.
 */
@Api(name = "leaderboard", version = "v1.1", namespace = @ApiNamespace(ownerDomain = "endpoint.gcm.kdoherty.com", ownerName = "endpoint.gcm.kdoherty.com", packagePath = ""))

public class LeaderboardEndpoint {

    private static final int MAX_ENTRIES = 5;

    private static final Logger log = Logger.getLogger(LeaderboardEndpoint.class.getName());

    static {
        log.setLevel(Level.INFO);
    }

    @ApiMethod(name = "getHighScores")
    public List<LeaderboardEntry> getHighScores() {

        log.info("Get high scores called");

        List<LeaderboardEntry> hs = ofy().load().type(LeaderboardEntry.class).limit(MAX_ENTRIES).list();

        for (LeaderboardEntry entry : hs) {
            log.info("LB Entry: " + entry.getScore());
        }

        return hs;
    }

    @ApiMethod(name = "submit")
    public void addEntry(@Named("name") String name, @Named("score")int score) {
        LeaderboardEntry entry = new LeaderboardEntry(name, score);

        log.info("LB Entry for name: " + name + " and score " + score + " added to Leaderboard");

        ofy().save().entity(entry).now();
    }
}
