package com.kdoherty.gcm.endpoint;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by kdoherty on 10/29/14.
 */
@Entity
public class LeaderboardEntry {

    @Id
    Long id;

    private int score;
    private String name;
    private String date;

    public LeaderboardEntry(String name, int score) {
        this.score = score;
        this.name = name;
        initDate();
    }

    public LeaderboardEntry() {
        initDate();
    }

    public int getScore() {
        return score;
    }

    public String getName() { return name; }

    public String getDate() { return date; }

    public void setScore(int score) {
         this.score = score;
    }

    public void setName(String name) { this.name = name; }

    public void setDate(String date) { this.date = date; }

    private void initDate() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        cal.add(Calendar.DATE, 1);
        this.date = cal.getTime().toString();
    }

}
