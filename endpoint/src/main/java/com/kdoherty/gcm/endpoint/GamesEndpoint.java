package com.kdoherty.gcm.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by kdoherty on 10/30/14.
 */
@Api(name = "games", version = "v1.1", namespace = @ApiNamespace(ownerDomain = "endpoint.gcm.kdoherty.com", ownerName = "endpoint.gcm.kdoherty.com", packagePath = ""))
public class GamesEndpoint {

    private Map<String, Game> gameMap = new HashMap<>();

    public Game createGame(@Named("regIdOne")String regIdOne, @Named("regIdTwo")String regIdTwo) {
        String gameId = UUID.randomUUID().toString();
        while (gameMap.containsKey(gameId)) {
            gameId = UUID.randomUUID().toString();
        }
        Game newGame = new Game(gameId, regIdOne, regIdTwo);
        gameMap.put(gameId, newGame);
        return newGame;
    }

    public void isFinished(@Named("gameId") String gameId, @Named("regId") String regId) {
        if (gameMap.containsKey(gameId)) {
            Game game = gameMap.get(gameId);
            boolean isOver = game.gameOver(regId);
            if (isOver) {
                gameMap.remove(gameId);
            }
        }
    }

    public void updateScore(@Named("gameId") String gameId,
                            @Named("regId") String regId,
                            @Named("score") int score) {

        if (gameMap.containsKey(gameId)) {
            Game game = gameMap.get(gameId);
            game.setScore(regId, score);
        }
    }
}
