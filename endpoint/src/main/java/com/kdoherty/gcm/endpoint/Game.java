package com.kdoherty.gcm.endpoint;

import java.io.IOException;

/**
 * Created by kdoherty on 10/26/14.
 */
public class Game {

    private static final MessagingEndpoint MSG_ENDPOINT = new MessagingEndpoint();

    private final String[] playerIds = new String[2];
    private final int[] scores = new int[2];
    private final boolean[] finished = new boolean[2];

    private String id;


    public Game(String id, String regIdOne, String regIdTwo) {
        this.id = id;
        this.playerIds[0] = regIdOne;
        this.playerIds[1] = regIdTwo;
    }

    public void setScore(String regId, int score) {
        int index = getIndex(regId);
        if (index == -1) {
            throw new RuntimeException("Invalid regId. Not in the right game");
        }
        scores[index] = score;

        try {
            MSG_ENDPOINT.sendScoreToId(score, playerIds[1-index]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public boolean gameOver(String regId) {
        int index = getIndex(regId);
        if (index == -1) {
            throw new RuntimeException("Invalid regId. Not in the right game. " + regId
                    + ". This games contains " + playerIds[0] + " and " + playerIds[1]);
        }
        // Hack to test playing against myself -- no other devices to test on
        if (finished[0] || finished[1]) {
            handleGameOver();
            return true;
        }
        finished[index] = true;
        if (finished[1 - index]) {
            handleGameOver();
            return true;
        }
        return false;
    }

    private void handleGameOver() {
        int winner = getWinnerIndex();
        int loser = 1 - winner;

        try {
            if (winner == -1) {
                for (int i = 0; i < playerIds.length; i++) {
                    MSG_ENDPOINT.sendMessageToIdNoIntent("Tie! Final score was " + scores[0] + "-" + scores[1],
                            playerIds[i]);
                    return;
                }
            } else {
                MSG_ENDPOINT.sendMessageToIdNoIntent("You win! Final score was "
                                + scores[winner] + "-" + scores[loser],
                        playerIds[winner]);
                MSG_ENDPOINT.sendMessageToIdNoIntent("You Lose... Final score was " +
                        scores[loser] + "-" + scores[winner], playerIds[loser]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getWinnerIndex() {
        if (scores[0] > scores[1]) {
            return 0;
        } else if (scores[1] > scores[0]) {
            return 1;
        }
        return -1;
    }

    private int getIndex(String regId) {
        if (playerIds.length > 0 && playerIds[0].equals(regId)) {
            return 0;
        } else if (playerIds.length > 1 && playerIds[1].equals(regId)) {
            return 1;
        }
        return -1;
    }

}
