package com.kdoherty.gcm.endpoint;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.kdoherty.cliche.endpoint.ClicheEntity;

/**
 * Objectify service wrapper so we can statically register our persistence classes
 * More on Objectify here : https://code.google.com/p/objectify-appengine/
 *
 */
public class OfyService {

    static {
        factory().register(RegistrationRecord.class);
        factory().register(LeaderboardEntry.class);
        factory().register(ClicheEntity.class);
    }   

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
