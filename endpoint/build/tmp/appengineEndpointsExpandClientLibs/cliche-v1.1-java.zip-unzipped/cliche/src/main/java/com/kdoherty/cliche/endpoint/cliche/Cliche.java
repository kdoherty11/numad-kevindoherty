/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2014-11-17 18:43:33 UTC)
 * on 2014-12-13 at 23:59:06 UTC 
 * Modify at your own risk.
 */

package com.kdoherty.cliche.endpoint.cliche;

/**
 * Service definition for Cliche (v1.1).
 *
 * <p>
 * This is an API
 * </p>
 *
 * <p>
 * For more information about this service, see the
 * <a href="" target="_blank">API Documentation</a>
 * </p>
 *
 * <p>
 * This service uses {@link ClicheRequestInitializer} to initialize global parameters via its
 * {@link Builder}.
 * </p>
 *
 * @since 1.3
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public class Cliche extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient {

  // Note: Leave this static initializer at the top of the file.
  static {
    com.google.api.client.util.Preconditions.checkState(
        com.google.api.client.googleapis.GoogleUtils.MAJOR_VERSION == 1 &&
        com.google.api.client.googleapis.GoogleUtils.MINOR_VERSION >= 15,
        "You are currently running with version %s of google-api-client. " +
        "You need at least version 1.15 of google-api-client to run version " +
        "1.19.0 of the cliche library.", com.google.api.client.googleapis.GoogleUtils.VERSION);
  }

  /**
   * The default encoded root URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_ROOT_URL = "https://solid-daylight-745.appspot.com/_ah/api/";

  /**
   * The default encoded service path of the service. This is determined when the library is
   * generated and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_SERVICE_PATH = "cliche/v1.1/";

  /**
   * The default encoded base URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   */
  public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

  /**
   * Constructor.
   *
   * <p>
   * Use {@link Builder} if you need to specify any of the optional parameters.
   * </p>
   *
   * @param transport HTTP transport, which should normally be:
   *        <ul>
   *        <li>Google App Engine:
   *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
   *        <li>Android: {@code newCompatibleTransport} from
   *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
   *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
   *        </li>
   *        </ul>
   * @param jsonFactory JSON factory, which may be:
   *        <ul>
   *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
   *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
   *        <li>Android Honeycomb or higher:
   *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
   *        </ul>
   * @param httpRequestInitializer HTTP request initializer or {@code null} for none
   * @since 1.7
   */
  public Cliche(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
      com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
    this(new Builder(transport, jsonFactory, httpRequestInitializer));
  }

  /**
   * @param builder builder
   */
  Cliche(Builder builder) {
    super(builder);
  }

  @Override
  protected void initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest<?> httpClientRequest) throws java.io.IOException {
    super.initialize(httpClientRequest);
  }

  /**
   * Create a request for the method "clearCliches".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link ClearCliches#execute()} method to invoke the remote operation.
   *
   * @return the request
   */
  public ClearCliches clearCliches() throws java.io.IOException {
    ClearCliches result = new ClearCliches();
    initialize(result);
    return result;
  }

  public class ClearCliches extends ClicheRequest<Void> {

    private static final String REST_PATH = "clearCliches";

    /**
     * Create a request for the method "clearCliches".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link ClearCliches#execute()} method to invoke the remote operation. <p>
     * {@link
     * ClearCliches#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @since 1.13
     */
    protected ClearCliches() {
      super(Cliche.this, "POST", REST_PATH, null, Void.class);
    }

    @Override
    public ClearCliches setAlt(java.lang.String alt) {
      return (ClearCliches) super.setAlt(alt);
    }

    @Override
    public ClearCliches setFields(java.lang.String fields) {
      return (ClearCliches) super.setFields(fields);
    }

    @Override
    public ClearCliches setKey(java.lang.String key) {
      return (ClearCliches) super.setKey(key);
    }

    @Override
    public ClearCliches setOauthToken(java.lang.String oauthToken) {
      return (ClearCliches) super.setOauthToken(oauthToken);
    }

    @Override
    public ClearCliches setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (ClearCliches) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public ClearCliches setQuotaUser(java.lang.String quotaUser) {
      return (ClearCliches) super.setQuotaUser(quotaUser);
    }

    @Override
    public ClearCliches setUserIp(java.lang.String userIp) {
      return (ClearCliches) super.setUserIp(userIp);
    }

    @Override
    public ClearCliches set(String parameterName, Object value) {
      return (ClearCliches) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "downVoteByClicheId".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link DownVoteByClicheId#execute()} method to invoke the remote operation.
   *
   * @param clicheId
   * @param userId
   * @return the request
   */
  public DownVoteByClicheId downVoteByClicheId(java.lang.Long clicheId, java.lang.String userId) throws java.io.IOException {
    DownVoteByClicheId result = new DownVoteByClicheId(clicheId, userId);
    initialize(result);
    return result;
  }

  public class DownVoteByClicheId extends ClicheRequest<Void> {

    private static final String REST_PATH = "downVoteByClicheId/{clicheId}/{userId}";

    /**
     * Create a request for the method "downVoteByClicheId".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link DownVoteByClicheId#execute()} method to invoke the remote
     * operation. <p> {@link DownVoteByClicheId#initialize(com.google.api.client.googleapis.services.A
     * bstractGoogleClientRequest)} must be called to initialize this instance immediately after
     * invoking the constructor. </p>
     *
     * @param clicheId
     * @param userId
     * @since 1.13
     */
    protected DownVoteByClicheId(java.lang.Long clicheId, java.lang.String userId) {
      super(Cliche.this, "POST", REST_PATH, null, Void.class);
      this.clicheId = com.google.api.client.util.Preconditions.checkNotNull(clicheId, "Required parameter clicheId must be specified.");
      this.userId = com.google.api.client.util.Preconditions.checkNotNull(userId, "Required parameter userId must be specified.");
    }

    @Override
    public DownVoteByClicheId setAlt(java.lang.String alt) {
      return (DownVoteByClicheId) super.setAlt(alt);
    }

    @Override
    public DownVoteByClicheId setFields(java.lang.String fields) {
      return (DownVoteByClicheId) super.setFields(fields);
    }

    @Override
    public DownVoteByClicheId setKey(java.lang.String key) {
      return (DownVoteByClicheId) super.setKey(key);
    }

    @Override
    public DownVoteByClicheId setOauthToken(java.lang.String oauthToken) {
      return (DownVoteByClicheId) super.setOauthToken(oauthToken);
    }

    @Override
    public DownVoteByClicheId setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (DownVoteByClicheId) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public DownVoteByClicheId setQuotaUser(java.lang.String quotaUser) {
      return (DownVoteByClicheId) super.setQuotaUser(quotaUser);
    }

    @Override
    public DownVoteByClicheId setUserIp(java.lang.String userIp) {
      return (DownVoteByClicheId) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.Long clicheId;

    /**

     */
    public java.lang.Long getClicheId() {
      return clicheId;
    }

    public DownVoteByClicheId setClicheId(java.lang.Long clicheId) {
      this.clicheId = clicheId;
      return this;
    }

    @com.google.api.client.util.Key
    private java.lang.String userId;

    /**

     */
    public java.lang.String getUserId() {
      return userId;
    }

    public DownVoteByClicheId setUserId(java.lang.String userId) {
      this.userId = userId;
      return this;
    }

    @Override
    public DownVoteByClicheId set(String parameterName, Object value) {
      return (DownVoteByClicheId) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "getCliches".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link GetCliches#execute()} method to invoke the remote operation.
   *
   * @return the request
   */
  public GetCliches getCliches() throws java.io.IOException {
    GetCliches result = new GetCliches();
    initialize(result);
    return result;
  }

  public class GetCliches extends ClicheRequest<com.kdoherty.cliche.endpoint.cliche.model.ClicheEntityCollection> {

    private static final String REST_PATH = "clicheentitycollection";

    /**
     * Create a request for the method "getCliches".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link GetCliches#execute()} method to invoke the remote operation. <p>
     * {@link
     * GetCliches#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @since 1.13
     */
    protected GetCliches() {
      super(Cliche.this, "GET", REST_PATH, null, com.kdoherty.cliche.endpoint.cliche.model.ClicheEntityCollection.class);
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public GetCliches setAlt(java.lang.String alt) {
      return (GetCliches) super.setAlt(alt);
    }

    @Override
    public GetCliches setFields(java.lang.String fields) {
      return (GetCliches) super.setFields(fields);
    }

    @Override
    public GetCliches setKey(java.lang.String key) {
      return (GetCliches) super.setKey(key);
    }

    @Override
    public GetCliches setOauthToken(java.lang.String oauthToken) {
      return (GetCliches) super.setOauthToken(oauthToken);
    }

    @Override
    public GetCliches setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (GetCliches) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public GetCliches setQuotaUser(java.lang.String quotaUser) {
      return (GetCliches) super.setQuotaUser(quotaUser);
    }

    @Override
    public GetCliches setUserIp(java.lang.String userIp) {
      return (GetCliches) super.setUserIp(userIp);
    }

    @Override
    public GetCliches set(String parameterName, Object value) {
      return (GetCliches) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "getClichesByUserId".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link GetClichesByUserId#execute()} method to invoke the remote operation.
   *
   * @param userId
   * @return the request
   */
  public GetClichesByUserId getClichesByUserId(java.lang.String userId) throws java.io.IOException {
    GetClichesByUserId result = new GetClichesByUserId(userId);
    initialize(result);
    return result;
  }

  public class GetClichesByUserId extends ClicheRequest<com.kdoherty.cliche.endpoint.cliche.model.ClicheEntityCollection> {

    private static final String REST_PATH = "clicheentitycollection/{userId}";

    /**
     * Create a request for the method "getClichesByUserId".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link GetClichesByUserId#execute()} method to invoke the remote
     * operation. <p> {@link GetClichesByUserId#initialize(com.google.api.client.googleapis.services.A
     * bstractGoogleClientRequest)} must be called to initialize this instance immediately after
     * invoking the constructor. </p>
     *
     * @param userId
     * @since 1.13
     */
    protected GetClichesByUserId(java.lang.String userId) {
      super(Cliche.this, "GET", REST_PATH, null, com.kdoherty.cliche.endpoint.cliche.model.ClicheEntityCollection.class);
      this.userId = com.google.api.client.util.Preconditions.checkNotNull(userId, "Required parameter userId must be specified.");
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public GetClichesByUserId setAlt(java.lang.String alt) {
      return (GetClichesByUserId) super.setAlt(alt);
    }

    @Override
    public GetClichesByUserId setFields(java.lang.String fields) {
      return (GetClichesByUserId) super.setFields(fields);
    }

    @Override
    public GetClichesByUserId setKey(java.lang.String key) {
      return (GetClichesByUserId) super.setKey(key);
    }

    @Override
    public GetClichesByUserId setOauthToken(java.lang.String oauthToken) {
      return (GetClichesByUserId) super.setOauthToken(oauthToken);
    }

    @Override
    public GetClichesByUserId setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (GetClichesByUserId) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public GetClichesByUserId setQuotaUser(java.lang.String quotaUser) {
      return (GetClichesByUserId) super.setQuotaUser(quotaUser);
    }

    @Override
    public GetClichesByUserId setUserIp(java.lang.String userIp) {
      return (GetClichesByUserId) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.String userId;

    /**

     */
    public java.lang.String getUserId() {
      return userId;
    }

    public GetClichesByUserId setUserId(java.lang.String userId) {
      this.userId = userId;
      return this;
    }

    @Override
    public GetClichesByUserId set(String parameterName, Object value) {
      return (GetClichesByUserId) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "getScore".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link GetScore#execute()} method to invoke the remote operation.
   *
   * @param userId
   * @return the request
   */
  public GetScore getScore(java.lang.String userId) throws java.io.IOException {
    GetScore result = new GetScore(userId);
    initialize(result);
    return result;
  }

  public class GetScore extends ClicheRequest<com.kdoherty.cliche.endpoint.cliche.model.JsonMap> {

    private static final String REST_PATH = "jsonobject/{userId}";

    /**
     * Create a request for the method "getScore".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link GetScore#execute()} method to invoke the remote operation. <p>
     * {@link
     * GetScore#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @param userId
     * @since 1.13
     */
    protected GetScore(java.lang.String userId) {
      super(Cliche.this, "GET", REST_PATH, null, com.kdoherty.cliche.endpoint.cliche.model.JsonMap.class);
      this.userId = com.google.api.client.util.Preconditions.checkNotNull(userId, "Required parameter userId must be specified.");
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public GetScore setAlt(java.lang.String alt) {
      return (GetScore) super.setAlt(alt);
    }

    @Override
    public GetScore setFields(java.lang.String fields) {
      return (GetScore) super.setFields(fields);
    }

    @Override
    public GetScore setKey(java.lang.String key) {
      return (GetScore) super.setKey(key);
    }

    @Override
    public GetScore setOauthToken(java.lang.String oauthToken) {
      return (GetScore) super.setOauthToken(oauthToken);
    }

    @Override
    public GetScore setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (GetScore) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public GetScore setQuotaUser(java.lang.String quotaUser) {
      return (GetScore) super.setQuotaUser(quotaUser);
    }

    @Override
    public GetScore setUserIp(java.lang.String userIp) {
      return (GetScore) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.String userId;

    /**

     */
    public java.lang.String getUserId() {
      return userId;
    }

    public GetScore setUserId(java.lang.String userId) {
      this.userId = userId;
      return this;
    }

    @Override
    public GetScore set(String parameterName, Object value) {
      return (GetScore) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "getUploadUrl".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link GetUploadUrl#execute()} method to invoke the remote operation.
   *
   * @return the request
   */
  public GetUploadUrl getUploadUrl() throws java.io.IOException {
    GetUploadUrl result = new GetUploadUrl();
    initialize(result);
    return result;
  }

  public class GetUploadUrl extends ClicheRequest<com.kdoherty.cliche.endpoint.cliche.model.JsonMap> {

    private static final String REST_PATH = "jsonobject";

    /**
     * Create a request for the method "getUploadUrl".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link GetUploadUrl#execute()} method to invoke the remote operation. <p>
     * {@link
     * GetUploadUrl#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @since 1.13
     */
    protected GetUploadUrl() {
      super(Cliche.this, "GET", REST_PATH, null, com.kdoherty.cliche.endpoint.cliche.model.JsonMap.class);
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public GetUploadUrl setAlt(java.lang.String alt) {
      return (GetUploadUrl) super.setAlt(alt);
    }

    @Override
    public GetUploadUrl setFields(java.lang.String fields) {
      return (GetUploadUrl) super.setFields(fields);
    }

    @Override
    public GetUploadUrl setKey(java.lang.String key) {
      return (GetUploadUrl) super.setKey(key);
    }

    @Override
    public GetUploadUrl setOauthToken(java.lang.String oauthToken) {
      return (GetUploadUrl) super.setOauthToken(oauthToken);
    }

    @Override
    public GetUploadUrl setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (GetUploadUrl) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public GetUploadUrl setQuotaUser(java.lang.String quotaUser) {
      return (GetUploadUrl) super.setQuotaUser(quotaUser);
    }

    @Override
    public GetUploadUrl setUserIp(java.lang.String userIp) {
      return (GetUploadUrl) super.setUserIp(userIp);
    }

    @Override
    public GetUploadUrl set(String parameterName, Object value) {
      return (GetUploadUrl) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "removeCliche".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link RemoveCliche#execute()} method to invoke the remote operation.
   *
   * @param clicheId
   * @return the request
   */
  public RemoveCliche removeCliche(java.lang.Long clicheId) throws java.io.IOException {
    RemoveCliche result = new RemoveCliche(clicheId);
    initialize(result);
    return result;
  }

  public class RemoveCliche extends ClicheRequest<Void> {

    private static final String REST_PATH = "cliche/{clicheId}";

    /**
     * Create a request for the method "removeCliche".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link RemoveCliche#execute()} method to invoke the remote operation. <p>
     * {@link
     * RemoveCliche#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @param clicheId
     * @since 1.13
     */
    protected RemoveCliche(java.lang.Long clicheId) {
      super(Cliche.this, "DELETE", REST_PATH, null, Void.class);
      this.clicheId = com.google.api.client.util.Preconditions.checkNotNull(clicheId, "Required parameter clicheId must be specified.");
    }

    @Override
    public RemoveCliche setAlt(java.lang.String alt) {
      return (RemoveCliche) super.setAlt(alt);
    }

    @Override
    public RemoveCliche setFields(java.lang.String fields) {
      return (RemoveCliche) super.setFields(fields);
    }

    @Override
    public RemoveCliche setKey(java.lang.String key) {
      return (RemoveCliche) super.setKey(key);
    }

    @Override
    public RemoveCliche setOauthToken(java.lang.String oauthToken) {
      return (RemoveCliche) super.setOauthToken(oauthToken);
    }

    @Override
    public RemoveCliche setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (RemoveCliche) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public RemoveCliche setQuotaUser(java.lang.String quotaUser) {
      return (RemoveCliche) super.setQuotaUser(quotaUser);
    }

    @Override
    public RemoveCliche setUserIp(java.lang.String userIp) {
      return (RemoveCliche) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.Long clicheId;

    /**

     */
    public java.lang.Long getClicheId() {
      return clicheId;
    }

    public RemoveCliche setClicheId(java.lang.Long clicheId) {
      this.clicheId = clicheId;
      return this;
    }

    @Override
    public RemoveCliche set(String parameterName, Object value) {
      return (RemoveCliche) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "storeCliche".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link StoreCliche#execute()} method to invoke the remote operation.
   *
   * @param content the {@link com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity}
   * @return the request
   */
  public StoreCliche storeCliche(com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity content) throws java.io.IOException {
    StoreCliche result = new StoreCliche(content);
    initialize(result);
    return result;
  }

  public class StoreCliche extends ClicheRequest<Void> {

    private static final String REST_PATH = "storeCliche";

    /**
     * Create a request for the method "storeCliche".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link StoreCliche#execute()} method to invoke the remote operation. <p>
     * {@link
     * StoreCliche#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @param content the {@link com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity}
     * @since 1.13
     */
    protected StoreCliche(com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity content) {
      super(Cliche.this, "POST", REST_PATH, content, Void.class);
    }

    @Override
    public StoreCliche setAlt(java.lang.String alt) {
      return (StoreCliche) super.setAlt(alt);
    }

    @Override
    public StoreCliche setFields(java.lang.String fields) {
      return (StoreCliche) super.setFields(fields);
    }

    @Override
    public StoreCliche setKey(java.lang.String key) {
      return (StoreCliche) super.setKey(key);
    }

    @Override
    public StoreCliche setOauthToken(java.lang.String oauthToken) {
      return (StoreCliche) super.setOauthToken(oauthToken);
    }

    @Override
    public StoreCliche setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (StoreCliche) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public StoreCliche setQuotaUser(java.lang.String quotaUser) {
      return (StoreCliche) super.setQuotaUser(quotaUser);
    }

    @Override
    public StoreCliche setUserIp(java.lang.String userIp) {
      return (StoreCliche) super.setUserIp(userIp);
    }

    @Override
    public StoreCliche set(String parameterName, Object value) {
      return (StoreCliche) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "upVoteByClicheId".
   *
   * This request holds the parameters needed by the cliche server.  After setting any optional
   * parameters, call the {@link UpVoteByClicheId#execute()} method to invoke the remote operation.
   *
   * @param clicheId
   * @param userId
   * @return the request
   */
  public UpVoteByClicheId upVoteByClicheId(java.lang.Long clicheId, java.lang.String userId) throws java.io.IOException {
    UpVoteByClicheId result = new UpVoteByClicheId(clicheId, userId);
    initialize(result);
    return result;
  }

  public class UpVoteByClicheId extends ClicheRequest<Void> {

    private static final String REST_PATH = "upVoteByClicheId/{clicheId}/{userId}";

    /**
     * Create a request for the method "upVoteByClicheId".
     *
     * This request holds the parameters needed by the the cliche server.  After setting any optional
     * parameters, call the {@link UpVoteByClicheId#execute()} method to invoke the remote operation.
     * <p> {@link UpVoteByClicheId#initialize(com.google.api.client.googleapis.services.AbstractGoogle
     * ClientRequest)} must be called to initialize this instance immediately after invoking the
     * constructor. </p>
     *
     * @param clicheId
     * @param userId
     * @since 1.13
     */
    protected UpVoteByClicheId(java.lang.Long clicheId, java.lang.String userId) {
      super(Cliche.this, "POST", REST_PATH, null, Void.class);
      this.clicheId = com.google.api.client.util.Preconditions.checkNotNull(clicheId, "Required parameter clicheId must be specified.");
      this.userId = com.google.api.client.util.Preconditions.checkNotNull(userId, "Required parameter userId must be specified.");
    }

    @Override
    public UpVoteByClicheId setAlt(java.lang.String alt) {
      return (UpVoteByClicheId) super.setAlt(alt);
    }

    @Override
    public UpVoteByClicheId setFields(java.lang.String fields) {
      return (UpVoteByClicheId) super.setFields(fields);
    }

    @Override
    public UpVoteByClicheId setKey(java.lang.String key) {
      return (UpVoteByClicheId) super.setKey(key);
    }

    @Override
    public UpVoteByClicheId setOauthToken(java.lang.String oauthToken) {
      return (UpVoteByClicheId) super.setOauthToken(oauthToken);
    }

    @Override
    public UpVoteByClicheId setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (UpVoteByClicheId) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public UpVoteByClicheId setQuotaUser(java.lang.String quotaUser) {
      return (UpVoteByClicheId) super.setQuotaUser(quotaUser);
    }

    @Override
    public UpVoteByClicheId setUserIp(java.lang.String userIp) {
      return (UpVoteByClicheId) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.Long clicheId;

    /**

     */
    public java.lang.Long getClicheId() {
      return clicheId;
    }

    public UpVoteByClicheId setClicheId(java.lang.Long clicheId) {
      this.clicheId = clicheId;
      return this;
    }

    @com.google.api.client.util.Key
    private java.lang.String userId;

    /**

     */
    public java.lang.String getUserId() {
      return userId;
    }

    public UpVoteByClicheId setUserId(java.lang.String userId) {
      this.userId = userId;
      return this;
    }

    @Override
    public UpVoteByClicheId set(String parameterName, Object value) {
      return (UpVoteByClicheId) super.set(parameterName, value);
    }
  }

  /**
   * Builder for {@link Cliche}.
   *
   * <p>
   * Implementation is not thread-safe.
   * </p>
   *
   * @since 1.3.0
   */
  public static final class Builder extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder {

    /**
     * Returns an instance of a new builder.
     *
     * @param transport HTTP transport, which should normally be:
     *        <ul>
     *        <li>Google App Engine:
     *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
     *        <li>Android: {@code newCompatibleTransport} from
     *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
     *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
     *        </li>
     *        </ul>
     * @param jsonFactory JSON factory, which may be:
     *        <ul>
     *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
     *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
     *        <li>Android Honeycomb or higher:
     *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
     *        </ul>
     * @param httpRequestInitializer HTTP request initializer or {@code null} for none
     * @since 1.7
     */
    public Builder(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
        com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      super(
          transport,
          jsonFactory,
          DEFAULT_ROOT_URL,
          DEFAULT_SERVICE_PATH,
          httpRequestInitializer,
          false);
    }

    /** Builds a new instance of {@link Cliche}. */
    @Override
    public Cliche build() {
      return new Cliche(this);
    }

    @Override
    public Builder setRootUrl(String rootUrl) {
      return (Builder) super.setRootUrl(rootUrl);
    }

    @Override
    public Builder setServicePath(String servicePath) {
      return (Builder) super.setServicePath(servicePath);
    }

    @Override
    public Builder setHttpRequestInitializer(com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
    }

    @Override
    public Builder setApplicationName(String applicationName) {
      return (Builder) super.setApplicationName(applicationName);
    }

    @Override
    public Builder setSuppressPatternChecks(boolean suppressPatternChecks) {
      return (Builder) super.setSuppressPatternChecks(suppressPatternChecks);
    }

    @Override
    public Builder setSuppressRequiredParameterChecks(boolean suppressRequiredParameterChecks) {
      return (Builder) super.setSuppressRequiredParameterChecks(suppressRequiredParameterChecks);
    }

    @Override
    public Builder setSuppressAllChecks(boolean suppressAllChecks) {
      return (Builder) super.setSuppressAllChecks(suppressAllChecks);
    }

    /**
     * Set the {@link ClicheRequestInitializer}.
     *
     * @since 1.12
     */
    public Builder setClicheRequestInitializer(
        ClicheRequestInitializer clicheRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(clicheRequestInitializer);
    }

    @Override
    public Builder setGoogleClientRequestInitializer(
        com.google.api.client.googleapis.services.GoogleClientRequestInitializer googleClientRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
    }
  }
}
