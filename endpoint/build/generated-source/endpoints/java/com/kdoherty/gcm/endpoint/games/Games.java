/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2014-11-17 18:43:33 UTC)
 * on 2014-12-13 at 23:59:04 UTC 
 * Modify at your own risk.
 */

package com.kdoherty.gcm.endpoint.games;

/**
 * Service definition for Games (v1.1).
 *
 * <p>
 * This is an API
 * </p>
 *
 * <p>
 * For more information about this service, see the
 * <a href="" target="_blank">API Documentation</a>
 * </p>
 *
 * <p>
 * This service uses {@link GamesRequestInitializer} to initialize global parameters via its
 * {@link Builder}.
 * </p>
 *
 * @since 1.3
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public class Games extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient {

  // Note: Leave this static initializer at the top of the file.
  static {
    com.google.api.client.util.Preconditions.checkState(
        com.google.api.client.googleapis.GoogleUtils.MAJOR_VERSION == 1 &&
        com.google.api.client.googleapis.GoogleUtils.MINOR_VERSION >= 15,
        "You are currently running with version %s of google-api-client. " +
        "You need at least version 1.15 of google-api-client to run version " +
        "1.19.0 of the games library.", com.google.api.client.googleapis.GoogleUtils.VERSION);
  }

  /**
   * The default encoded root URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_ROOT_URL = "https://solid-daylight-745.appspot.com/_ah/api/";

  /**
   * The default encoded service path of the service. This is determined when the library is
   * generated and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_SERVICE_PATH = "games/v1.1/";

  /**
   * The default encoded base URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   */
  public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

  /**
   * Constructor.
   *
   * <p>
   * Use {@link Builder} if you need to specify any of the optional parameters.
   * </p>
   *
   * @param transport HTTP transport, which should normally be:
   *        <ul>
   *        <li>Google App Engine:
   *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
   *        <li>Android: {@code newCompatibleTransport} from
   *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
   *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
   *        </li>
   *        </ul>
   * @param jsonFactory JSON factory, which may be:
   *        <ul>
   *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
   *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
   *        <li>Android Honeycomb or higher:
   *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
   *        </ul>
   * @param httpRequestInitializer HTTP request initializer or {@code null} for none
   * @since 1.7
   */
  public Games(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
      com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
    this(new Builder(transport, jsonFactory, httpRequestInitializer));
  }

  /**
   * @param builder builder
   */
  Games(Builder builder) {
    super(builder);
  }

  @Override
  protected void initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest<?> httpClientRequest) throws java.io.IOException {
    super.initialize(httpClientRequest);
  }

  /**
   * An accessor for creating requests from the GamesEndpoint collection.
   *
   * <p>The typical use is:</p>
   * <pre>
   *   {@code Games games = new Games(...);}
   *   {@code Games.GamesEndpoint.List request = games.gamesEndpoint().list(parameters ...)}
   * </pre>
   *
   * @return the resource collection
   */
  public GamesEndpoint gamesEndpoint() {
    return new GamesEndpoint();
  }

  /**
   * The "gamesEndpoint" collection of methods.
   */
  public class GamesEndpoint {

    /**
     * Create a request for the method "gamesEndpoint.createGame".
     *
     * This request holds the parameters needed by the games server.  After setting any optional
     * parameters, call the {@link CreateGame#execute()} method to invoke the remote operation.
     *
     * @param regIdOne
     * @param regIdTwo
     * @return the request
     */
    public CreateGame createGame(java.lang.String regIdOne, java.lang.String regIdTwo) throws java.io.IOException {
      CreateGame result = new CreateGame(regIdOne, regIdTwo);
      initialize(result);
      return result;
    }

    public class CreateGame extends GamesRequest<com.kdoherty.gcm.endpoint.games.model.Game> {

      private static final String REST_PATH = "createGame/{regIdOne}/{regIdTwo}";

      /**
       * Create a request for the method "gamesEndpoint.createGame".
       *
       * This request holds the parameters needed by the the games server.  After setting any optional
       * parameters, call the {@link CreateGame#execute()} method to invoke the remote operation. <p>
       * {@link
       * CreateGame#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
       * must be called to initialize this instance immediately after invoking the constructor. </p>
       *
       * @param regIdOne
       * @param regIdTwo
       * @since 1.13
       */
      protected CreateGame(java.lang.String regIdOne, java.lang.String regIdTwo) {
        super(Games.this, "POST", REST_PATH, null, com.kdoherty.gcm.endpoint.games.model.Game.class);
        this.regIdOne = com.google.api.client.util.Preconditions.checkNotNull(regIdOne, "Required parameter regIdOne must be specified.");
        this.regIdTwo = com.google.api.client.util.Preconditions.checkNotNull(regIdTwo, "Required parameter regIdTwo must be specified.");
      }

      @Override
      public CreateGame setAlt(java.lang.String alt) {
        return (CreateGame) super.setAlt(alt);
      }

      @Override
      public CreateGame setFields(java.lang.String fields) {
        return (CreateGame) super.setFields(fields);
      }

      @Override
      public CreateGame setKey(java.lang.String key) {
        return (CreateGame) super.setKey(key);
      }

      @Override
      public CreateGame setOauthToken(java.lang.String oauthToken) {
        return (CreateGame) super.setOauthToken(oauthToken);
      }

      @Override
      public CreateGame setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (CreateGame) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public CreateGame setQuotaUser(java.lang.String quotaUser) {
        return (CreateGame) super.setQuotaUser(quotaUser);
      }

      @Override
      public CreateGame setUserIp(java.lang.String userIp) {
        return (CreateGame) super.setUserIp(userIp);
      }

      @com.google.api.client.util.Key
      private java.lang.String regIdOne;

      /**

       */
      public java.lang.String getRegIdOne() {
        return regIdOne;
      }

      public CreateGame setRegIdOne(java.lang.String regIdOne) {
        this.regIdOne = regIdOne;
        return this;
      }

      @com.google.api.client.util.Key
      private java.lang.String regIdTwo;

      /**

       */
      public java.lang.String getRegIdTwo() {
        return regIdTwo;
      }

      public CreateGame setRegIdTwo(java.lang.String regIdTwo) {
        this.regIdTwo = regIdTwo;
        return this;
      }

      @Override
      public CreateGame set(String parameterName, Object value) {
        return (CreateGame) super.set(parameterName, value);
      }
    }
    /**
     * Create a request for the method "gamesEndpoint.isFinished".
     *
     * This request holds the parameters needed by the games server.  After setting any optional
     * parameters, call the {@link IsFinished#execute()} method to invoke the remote operation.
     *
     * @param gameId
     * @param regId
     * @return the request
     */
    public IsFinished isFinished(java.lang.String gameId, java.lang.String regId) throws java.io.IOException {
      IsFinished result = new IsFinished(gameId, regId);
      initialize(result);
      return result;
    }

    public class IsFinished extends GamesRequest<Void> {

      private static final String REST_PATH = "isFinished/{gameId}/{regId}";

      /**
       * Create a request for the method "gamesEndpoint.isFinished".
       *
       * This request holds the parameters needed by the the games server.  After setting any optional
       * parameters, call the {@link IsFinished#execute()} method to invoke the remote operation. <p>
       * {@link
       * IsFinished#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
       * must be called to initialize this instance immediately after invoking the constructor. </p>
       *
       * @param gameId
       * @param regId
       * @since 1.13
       */
      protected IsFinished(java.lang.String gameId, java.lang.String regId) {
        super(Games.this, "POST", REST_PATH, null, Void.class);
        this.gameId = com.google.api.client.util.Preconditions.checkNotNull(gameId, "Required parameter gameId must be specified.");
        this.regId = com.google.api.client.util.Preconditions.checkNotNull(regId, "Required parameter regId must be specified.");
      }

      @Override
      public IsFinished setAlt(java.lang.String alt) {
        return (IsFinished) super.setAlt(alt);
      }

      @Override
      public IsFinished setFields(java.lang.String fields) {
        return (IsFinished) super.setFields(fields);
      }

      @Override
      public IsFinished setKey(java.lang.String key) {
        return (IsFinished) super.setKey(key);
      }

      @Override
      public IsFinished setOauthToken(java.lang.String oauthToken) {
        return (IsFinished) super.setOauthToken(oauthToken);
      }

      @Override
      public IsFinished setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (IsFinished) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public IsFinished setQuotaUser(java.lang.String quotaUser) {
        return (IsFinished) super.setQuotaUser(quotaUser);
      }

      @Override
      public IsFinished setUserIp(java.lang.String userIp) {
        return (IsFinished) super.setUserIp(userIp);
      }

      @com.google.api.client.util.Key
      private java.lang.String gameId;

      /**

       */
      public java.lang.String getGameId() {
        return gameId;
      }

      public IsFinished setGameId(java.lang.String gameId) {
        this.gameId = gameId;
        return this;
      }

      @com.google.api.client.util.Key
      private java.lang.String regId;

      /**

       */
      public java.lang.String getRegId() {
        return regId;
      }

      public IsFinished setRegId(java.lang.String regId) {
        this.regId = regId;
        return this;
      }

      @Override
      public IsFinished set(String parameterName, Object value) {
        return (IsFinished) super.set(parameterName, value);
      }
    }
    /**
     * Create a request for the method "gamesEndpoint.updateScore".
     *
     * This request holds the parameters needed by the games server.  After setting any optional
     * parameters, call the {@link UpdateScore#execute()} method to invoke the remote operation.
     *
     * @param gameId
     * @param regId
     * @param score
     * @return the request
     */
    public UpdateScore updateScore(java.lang.String gameId, java.lang.String regId, java.lang.Integer score) throws java.io.IOException {
      UpdateScore result = new UpdateScore(gameId, regId, score);
      initialize(result);
      return result;
    }

    public class UpdateScore extends GamesRequest<Void> {

      private static final String REST_PATH = "void/{gameId}/{regId}/{score}";

      /**
       * Create a request for the method "gamesEndpoint.updateScore".
       *
       * This request holds the parameters needed by the the games server.  After setting any optional
       * parameters, call the {@link UpdateScore#execute()} method to invoke the remote operation. <p>
       * {@link
       * UpdateScore#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
       * must be called to initialize this instance immediately after invoking the constructor. </p>
       *
       * @param gameId
       * @param regId
       * @param score
       * @since 1.13
       */
      protected UpdateScore(java.lang.String gameId, java.lang.String regId, java.lang.Integer score) {
        super(Games.this, "PUT", REST_PATH, null, Void.class);
        this.gameId = com.google.api.client.util.Preconditions.checkNotNull(gameId, "Required parameter gameId must be specified.");
        this.regId = com.google.api.client.util.Preconditions.checkNotNull(regId, "Required parameter regId must be specified.");
        this.score = com.google.api.client.util.Preconditions.checkNotNull(score, "Required parameter score must be specified.");
      }

      @Override
      public UpdateScore setAlt(java.lang.String alt) {
        return (UpdateScore) super.setAlt(alt);
      }

      @Override
      public UpdateScore setFields(java.lang.String fields) {
        return (UpdateScore) super.setFields(fields);
      }

      @Override
      public UpdateScore setKey(java.lang.String key) {
        return (UpdateScore) super.setKey(key);
      }

      @Override
      public UpdateScore setOauthToken(java.lang.String oauthToken) {
        return (UpdateScore) super.setOauthToken(oauthToken);
      }

      @Override
      public UpdateScore setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (UpdateScore) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public UpdateScore setQuotaUser(java.lang.String quotaUser) {
        return (UpdateScore) super.setQuotaUser(quotaUser);
      }

      @Override
      public UpdateScore setUserIp(java.lang.String userIp) {
        return (UpdateScore) super.setUserIp(userIp);
      }

      @com.google.api.client.util.Key
      private java.lang.String gameId;

      /**

       */
      public java.lang.String getGameId() {
        return gameId;
      }

      public UpdateScore setGameId(java.lang.String gameId) {
        this.gameId = gameId;
        return this;
      }

      @com.google.api.client.util.Key
      private java.lang.String regId;

      /**

       */
      public java.lang.String getRegId() {
        return regId;
      }

      public UpdateScore setRegId(java.lang.String regId) {
        this.regId = regId;
        return this;
      }

      @com.google.api.client.util.Key
      private java.lang.Integer score;

      /**

       */
      public java.lang.Integer getScore() {
        return score;
      }

      public UpdateScore setScore(java.lang.Integer score) {
        this.score = score;
        return this;
      }

      @Override
      public UpdateScore set(String parameterName, Object value) {
        return (UpdateScore) super.set(parameterName, value);
      }
    }

  }

  /**
   * Builder for {@link Games}.
   *
   * <p>
   * Implementation is not thread-safe.
   * </p>
   *
   * @since 1.3.0
   */
  public static final class Builder extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder {

    /**
     * Returns an instance of a new builder.
     *
     * @param transport HTTP transport, which should normally be:
     *        <ul>
     *        <li>Google App Engine:
     *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
     *        <li>Android: {@code newCompatibleTransport} from
     *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
     *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
     *        </li>
     *        </ul>
     * @param jsonFactory JSON factory, which may be:
     *        <ul>
     *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
     *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
     *        <li>Android Honeycomb or higher:
     *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
     *        </ul>
     * @param httpRequestInitializer HTTP request initializer or {@code null} for none
     * @since 1.7
     */
    public Builder(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
        com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      super(
          transport,
          jsonFactory,
          DEFAULT_ROOT_URL,
          DEFAULT_SERVICE_PATH,
          httpRequestInitializer,
          false);
    }

    /** Builds a new instance of {@link Games}. */
    @Override
    public Games build() {
      return new Games(this);
    }

    @Override
    public Builder setRootUrl(String rootUrl) {
      return (Builder) super.setRootUrl(rootUrl);
    }

    @Override
    public Builder setServicePath(String servicePath) {
      return (Builder) super.setServicePath(servicePath);
    }

    @Override
    public Builder setHttpRequestInitializer(com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
    }

    @Override
    public Builder setApplicationName(String applicationName) {
      return (Builder) super.setApplicationName(applicationName);
    }

    @Override
    public Builder setSuppressPatternChecks(boolean suppressPatternChecks) {
      return (Builder) super.setSuppressPatternChecks(suppressPatternChecks);
    }

    @Override
    public Builder setSuppressRequiredParameterChecks(boolean suppressRequiredParameterChecks) {
      return (Builder) super.setSuppressRequiredParameterChecks(suppressRequiredParameterChecks);
    }

    @Override
    public Builder setSuppressAllChecks(boolean suppressAllChecks) {
      return (Builder) super.setSuppressAllChecks(suppressAllChecks);
    }

    /**
     * Set the {@link GamesRequestInitializer}.
     *
     * @since 1.12
     */
    public Builder setGamesRequestInitializer(
        GamesRequestInitializer gamesRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(gamesRequestInitializer);
    }

    @Override
    public Builder setGoogleClientRequestInitializer(
        com.google.api.client.googleapis.services.GoogleClientRequestInitializer googleClientRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
    }
  }
}
