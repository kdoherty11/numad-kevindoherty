package edu.neu.madcourse.kevindoherty.bananagrams.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.kevindoherty.dictionary.Dictionary;

/**
 * Created by kdoherty on 10/2/14.
 */
public final class Board implements Parcelable {

    public static final int NUM_ROWS = 50;
    public static final int NUM_COLS = 50;

    private final Tile[][] tiles = new Tile[NUM_ROWS][NUM_COLS];

    public Board() {
        fillWithEmptyTiles();
    }

    private void fillWithEmptyTiles() {
        for (int i = 0; i < Board.NUM_ROWS; i++) {
            for (int j = 0; j < Board.NUM_COLS; j++) {
                tiles[i][j] = Tile.MT;
            }
        }
    }

    private static boolean isInbounds(int row, int col) {
        return row >= 0 && row < NUM_ROWS
                && col >= 0 && col < NUM_COLS;
    }

    private static void throwOutOfBounds(int row, int col) {
        throw new ArrayIndexOutOfBoundsException
                ("can't access row: " + row + " and col: " + col);
    }

    private static void handleInbounds(int row, int col) {
        if (!isInbounds(row, col)) {
            throwOutOfBounds(row, col);
        }
    }

    public Tile getTile(int row, int col) {
        handleInbounds(row, col);
        return tiles[row][col];
    }

    public Tile placeTile(int row, int col, Tile tile) {
        handleInbounds(row, col);
        Tile temp = getTile(row, col);
        tiles[row][col] = tile;
        return temp;
    }

    public Tile removeTile(int row, int col) {
        handleInbounds(row, col);
        Tile temp = tiles[row][col];
        tiles[row][col] = Tile.MT;
        return temp;
    }

    public void swapTiles(int row1, int col1, int row2, int col2) {
        // Inbounds checks are done in get tile calls
        Tile temp = getTile(row1, col1);
        placeTile(row1, col1, getTile(row2, col2));
        placeTile(row2, col2, temp);
    }

    public boolean isEmpty(int row, int col) {
        return getTile(row, col) == Tile.MT;
    }

    public int calculateScore(Dictionary dictionary) {
        int score = 0;

        List<List<Tile>> words = getAllValidWords(dictionary);
        for (List<Tile> word : words) {
            score += tilesToPoints(word);
        }

        return score;
    }

    public boolean areAllValidWords(Dictionary dictionary) {
        List<List<Tile>> words = getAllWords();

        for (List<Tile> word : words) {
            if (!dictionary.isWord(tilesToString(word))) {
                return false;
            }
        }

        return true;
    }

    private boolean isHorizontalStart(int row, int col) {
        if (isEmpty(row, col)) {
            return false;
        }
        boolean leftEmpty = isInbounds(row, col - 1) && isEmpty(row, col - 1);
        boolean rightFull = isInbounds(row, col + 1) && !isEmpty(row, col + 1);
        return leftEmpty && rightFull;
    }

    private boolean isVerticalStart(int row, int col) {
        if (isEmpty(row, col)) {
            return false;
        }
        boolean topEmpty = isInbounds(row - 1, col) && isEmpty(row - 1, col);
        boolean bottomFull = isInbounds(row + 1, col) && !isEmpty(row + 1, col);
        return topEmpty && bottomFull;
    }

    private List<Tile> getHorizontalWord(int row, int col) {
        List<Tile> word = new ArrayList<>();
        Tile tile = getTile(row, col);
        assert (tile != Tile.MT);
        word.add(tile);

        while (isInbounds(row, ++col)) {
            tile = getTile(row, col);
            if (tile == Tile.MT) {
                break;
            }
            word.add(tile);
        }

        return word;
    }

    private List<Tile> getVerticalWord(int row, int col) {
        List<Tile> word = new ArrayList<>();
        Tile tile = getTile(row, col);
        assert (tile != Tile.MT);
        word.add(tile);

        while (isInbounds(++row, col)) {
            tile = getTile(row, col);
            if (tile == Tile.MT) {
                break;
            }
            word.add(tile);
        }

        return word;
    }

    public static String tilesToString(List<Tile> tiles) {
        StringBuilder sb = new StringBuilder(tiles.size());
        for (Tile tile : tiles) {
            assert(tile != Tile.MT);
            sb.append(tile.getLetter());
        }
        return sb.toString();
    }

    public static int tilesToPoints(List<Tile> tiles) {

        int points = 0;

        for (Tile tile : tiles) {
            points += tile.getPoints();
        }

        return points;
    }

    public List<List<Tile>> getAllWords() {
        List<List<Tile>> words = new ArrayList<>();
        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                if (getTile(i, j) != Tile.MT) {
                    if (isHorizontalStart(i, j)) {
                        words.add(getHorizontalWord(i, j));
                    }
                    if (isVerticalStart(i, j)) {
                        words.add(getVerticalWord(i, j));
                    }
                }
            }
        }
        return words;
    }

    public List<List<Tile>> getAllValidWords(Dictionary dictionary) {
        List<List<Tile>> words = getAllWords();
        List<List<Tile>> validWords = new ArrayList<>();

        for (List<Tile> word : words) {
            if (dictionary.isWord(tilesToString(word))) {
                validWords.add(word);
            }
        }

        return validWords;
    }

    public boolean isAllConnected() {
        boolean[][] connected = new boolean[NUM_ROWS][NUM_COLS];
        boolean marked = false;

        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                if (getTile(i, j) != Tile.MT) {
                    if (!marked) {
                        connected = markConnectedTiles(i, j, connected);
                        marked = true;
                    } else {
                        if (!connected[i][j]) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean[][] markConnectedTiles(int row, int col, boolean[][] marks) {

        // Mark first tile
        if (getTile(row, col) != Tile.MT && !marks[row][col]) {
            marks[row][col] = true;
        }

        // recur on all surrounding tiles
        for (int i = row - 1; i < row + 2; i++) {
            for (int j = col - 1; j < col + 2; j++) {
                if (isInbounds(i, j) && !(i == row && j == col) && getTile(i, j) != Tile.MT && !marks[i][j]) {
                    marks = markConnectedTiles(i, j, marks);
                }
            }
        }
        return marks;
    }

    public boolean isHorizontalWord(int row, int col) {
        return !isEmpty(row, col) && !(isEmpty(row, col - 1) && isEmpty(row, col + 1));
    }

    public boolean isVerticalWord(int row, int col) {
        return !isEmpty(row, col) && !(isEmpty(row + 1, col) && isEmpty(row - 1, col));
    }

    public List<Tile> getHorizontalTiles(int row, int col) {
        List<Tile> tiles = new ArrayList<>();
        List<Tile> rightTiles = getHorizontalWord(row, col);
        while (isInbounds(row, --col)) {
            Tile tile = getTile(row, col);
            if (tile == Tile.MT) {
                break;
            }
            System.out.println("Adding horizontal tile: " + tile);
            tiles.add(0, tile);
        }

        System.out.println("Horizontal tiles before adding all: " + tiles);

        tiles.addAll(rightTiles);

        System.out.println("Horizontal tiles after adding all: " + tiles);

        return tiles;
    }

    public List<Tile> getVerticalTiles(int row, int col) {
        List<Tile> tiles = new ArrayList<>();
        List<Tile> belowTiles = getVerticalWord(row, col);
        while (isInbounds(--row, col)) {
            Tile tile = getTile(row, col);
            if (tile == Tile.MT) {
                break;
            }
            System.out.println("Adding vertical tile: " + tile);
            tiles.add(0, tile);
        }

        tiles.addAll(belowTiles);

        return tiles;
    }

    public List<List<Tile>> getWordsFromSquare(int row, int col) {
        List<List<Tile>> words = new ArrayList<>();
        if (isHorizontalWord(row, col)) {
            words.add(getHorizontalTiles(row, col));
        }
        if (isVerticalWord(row, col)) {
            words.add(getVerticalTiles(row, col));
        }
        return words;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void clearBoard() {
        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                removeTile(i, j);
            }
        }
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        System.out.println("Writing Board \n" + this);
        for (int i = 0; i < NUM_ROWS; i++) {
             out.writeParcelableArray(tiles[i], flags);
        }
    }

    public static final Parcelable.Creator<Board> CREATOR
            = new Parcelable.Creator<Board>() {
        public Board createFromParcel(Parcel in) {
            return new Board(in);
        }

        public Board[] newArray(int size) {
            return new Board[size];
        }
    };

    private Board(Parcel in) {
        clearBoard();
        for (int i = 0; i < NUM_ROWS; i++) {
            tiles[i] = (Tile[]) in.readParcelableArray(Tile.class.getClassLoader());
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder colNum = new StringBuilder("  ");
        for (int j = 0; j < NUM_COLS; j++) {
            colNum.append(j);
            colNum.append(" ");
        }
        colNum.append("\n");
        for (int i = 0; i < NUM_ROWS; i++) {
            sb.append(i);
            sb.append("|");
            for (int j = 0; j < NUM_COLS; j++) {
                if (!isEmpty(i, j)) {
                    sb.append(getTile(i, j));
                } else {
                    sb.append(" ");
                }
                sb.append("|");
            }
            sb.append("\n");
        }
        sb.append("\n");
        sb.insert(0, colNum.toString());
        return sb.toString();
    }
}
