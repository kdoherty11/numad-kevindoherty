package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.util.DateTime;
import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import edu.neu.madcourse.kevindoherty.R;

/**
 * Created by kdoherty on 11/14/14.
 */
public class ClicheAdapter extends ArrayAdapter<ClicheEntity> {

    private static final String TAG = "All Cliches Adapter";

    List<ClicheEntity> entries;
    Context context;

    private float mScale;

    private static final ImageLoader imageLoader = ImageLoader.getInstance();

    public ClicheAdapter(Context context, int resource, List<ClicheEntity> entries) {
        super(context, resource, entries);
        this.entries = entries;
        this.context = context;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        mScale = getContext().getResources().getDisplayMetrics().density;
    }

    static class ViewHolder {
        ImageView picture;
        ImageView bonusPicture;
        TextView score;
        TextView clicheStr;
        TextView comment;
        ImageView upVote;
        ImageView downVote;
        TextView timeAgo;

        public ViewHolder(ImageView picture, ImageView bonusPicture, TextView score, TextView clicheStr,
                          TextView comment, ImageView upVote, ImageView downVote, TextView timeAgo) {
            this.picture = picture;
            this.bonusPicture = bonusPicture;
            this.score = score;
            this.clicheStr = clicheStr;
            this.comment = comment;
            this.upVote = upVote;
            this.downVote = downVote;
            this.timeAgo = timeAgo;
        }
    }

    public static enum ClicheVoteComparator implements Comparator<ClicheEntity> {

        INSTANCE;

        @Override
        public int compare(ClicheEntity clicheEntity, ClicheEntity clicheEntity2) {
            int score1 = clicheEntity.getScore();
            int score2 = clicheEntity2.getScore();
            return score2 - score1;
        }
    }

    /**
     * Most recent is better
     */
    public static enum ClicheDateComparator implements Comparator<ClicheEntity> {

        INSTANCE;

        @Override
        public int compare(ClicheEntity o1, ClicheEntity o2) {

            com.google.api.client.util.DateTime date1 = o1.getTimeStamp();
            com.google.api.client.util.DateTime date2 = o2.getTimeStamp();
            Date d1 = new Date(date1.getValue());
            Date d2 = new Date(date2.getValue());

            return d2.compareTo(d1);
        }
    }



    public void sortByDescendingVotes() {
        Collections.sort(entries, ClicheVoteComparator.INSTANCE);
        notifyDataSetChanged();
    }

    public void sortByMostRecent() {
        Collections.sort(entries, ClicheDateComparator.INSTANCE);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        final ImageView pictureIv;
        final ImageView bonusPictureIv;
        final TextView scoreTv;
        final TextView clicheStrTv;
        final TextView commentTv;
        final ImageView upVote;
        final ImageView downVote;
        final TextView timeAgoTv;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cliche, null);
            pictureIv = (ImageView) convertView.findViewById(R.id.picture);
            bonusPictureIv = (ImageView) convertView.findViewById(R.id.bonus_picture);
            scoreTv = (TextView) convertView.findViewById(R.id.score);
            clicheStrTv = (TextView) convertView.findViewById(R.id.cliche_string);
            commentTv = (TextView) convertView.findViewById(R.id.comment);
            upVote = (ImageView) convertView.findViewById(R.id.up_vote);
            downVote = (ImageView) convertView.findViewById(R.id.down_vote);
            timeAgoTv = (TextView) convertView.findViewById(R.id.time_ago);
            viewHolder = new ViewHolder(pictureIv, bonusPictureIv, scoreTv, clicheStrTv,
                    commentTv, upVote, downVote, timeAgoTv);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            pictureIv = viewHolder.picture;
            bonusPictureIv = viewHolder.bonusPicture;
            scoreTv = viewHolder.score;
            clicheStrTv = viewHolder.clicheStr;
            commentTv = viewHolder.comment;
            upVote = viewHolder.upVote;
            downVote = viewHolder.downVote;
            timeAgoTv = viewHolder.timeAgo;
        }

        ClicheEntity cliche = entries.get(position);
        final long clicheId = cliche.getId();
        final int startingScore = cliche.getScore();
        int score = startingScore;

        List<String> tempVotedOn = cliche.getVotedOn();
        List<String> result;

        if (tempVotedOn == null) {
            Log.d(TAG, "VotedOn is null");
            result = new ArrayList<>();
        } else {
            Log.d(TAG, "Voted on is NOT null!");
            Log.d(TAG, "VotedOn: " + tempVotedOn);
            result = new ArrayList<>(tempVotedOn);
        }

        final List<String> votedOn = result;

        upVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String myUserId = ClicheUtils.getUserId(context);

                if (votedOn.contains(myUserId) || VoteCliche.votedOn.contains(clicheId)) {
                    Toast.makeText(context, "You already voted on this cliche", Toast.LENGTH_SHORT).show();
                } else {
                    VoteCliche.votedOn.add(clicheId);
                    new VoteCliche(clicheId, myUserId, VoteCliche.VoteType.UP).execute();
                    scoreTv.setText(String.valueOf(startingScore + 1));
                }
            }
        });

        downVote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String myUserId = ClicheUtils.getUserId(context);

                if (votedOn.contains(myUserId) || VoteCliche.votedOn.contains(clicheId)) {
                    Toast.makeText(context, "You already voted on this cliche", Toast.LENGTH_SHORT).show();
                } else {
                    VoteCliche.votedOn.add(clicheId);
                    new VoteCliche(clicheId, myUserId, VoteCliche.VoteType.DOWN).execute();
                    scoreTv.setText(String.valueOf(startingScore - 1));
                }
            }
        });

        final String servingUrl = cliche.getServingUrl();
        final String extraPictureServingUrl = cliche.getExtraPictureServingUrl();
        final String clicheStr = cliche.getCliche();
        final String comment = cliche.getComment();
        final String scoreStr = String.valueOf(score);
        final DateTime timeStamp = cliche.getTimeStamp();

        if (Strings.isNullOrEmpty(extraPictureServingUrl)) {
            // Only display servingUrl pic
            String squarePicServingUrl = servingUrl + "=s250-c";
            imageLoader.displayImage(squarePicServingUrl, pictureIv);
            pictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, servingUrl));
            bonusPictureIv.setVisibility(View.GONE);
            // Want to set picture dimensions to 100 dp
            int dimDps = 100;
            // Convert dp to pixels for layout params
            int dimPx = (int) (dimDps * mScale + 0.5f);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dimPx, dimPx);
            pictureIv.setLayoutParams(params);
        } else {
            // Display serving and extra pic
            imageLoader.displayImage(servingUrl, pictureIv);
            imageLoader.displayImage(extraPictureServingUrl, bonusPictureIv);
            pictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, servingUrl));
            bonusPictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, extraPictureServingUrl));
        }

        if (!Strings.isNullOrEmpty(clicheStr)) {
            clicheStrTv.setText(clicheStr);
        }

        if (!Strings.isNullOrEmpty(comment)) {
            commentTv.setText(comment);
        }

        if (!Strings.isNullOrEmpty(scoreStr)) {
            scoreTv.setText(scoreStr);
        }

        if (timeStamp != null) {
            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(timeStamp.getValue());
            if (timeAgoTv == null) {
                Log.e(TAG, "timeAgoTv is null...");
            } else {
                timeAgoTv.setText(timeAgo);
            }
        }

        return convertView;
    }

}
