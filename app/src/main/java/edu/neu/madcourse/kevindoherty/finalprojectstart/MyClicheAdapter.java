package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.util.DateTime;
import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import edu.neu.madcourse.kevindoherty.R;

/**
 * Created by kdoherty on 11/29/14.
 */
public class MyClicheAdapter extends ArrayAdapter<ClicheEntity> {

    private static final String TAG = "My Cliches Adapter";

    List<ClicheEntity> entries;
    Context context;
    private float mScale;

    private static final ImageLoader imageLoader = ImageLoader.getInstance();

    public MyClicheAdapter(Context context, int resource, List<ClicheEntity> entries) {
        super(context, resource, entries);
        this.entries = entries;
        this.context = context;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        mScale = getContext().getResources().getDisplayMetrics().density;
    }

    private void removeCliche(final long clicheId) {

        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    Endpoints.CLICHE_API.removeCliche(clicheId).execute();
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Problem removing cliche with id: " + clicheId +
                            " ERROR: " + e.getMessage());
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) {
                    int indexOfCliche = indexOf(clicheId);
                    if (indexOfCliche == -1) {
                        Log.e(TAG, "Called remove on cliche which is not in adapter's entry list");
                    }
                    entries.remove(indexOfCliche);
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Cliche could not be removed. Please try again.",
                            Toast.LENGTH_SHORT).show();
                }
            }

        }.execute();

    }

    private int indexOf(long clicheId) {
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).getId().equals(clicheId)) {
                return i;
            }
        }
        return -1;
    }

    static class ViewHolder {
        ImageView picture;
        ImageView bonusPicture;
        TextView score;
        TextView clicheStr;
        TextView comment;
        ImageView trash;
        TextView timeAgo;

        public ViewHolder(ImageView picture, ImageView bonusPicture, TextView score, TextView clicheStr,
                          TextView comment, ImageView trash, TextView timeAgo) {
            this.picture = picture;
            this.bonusPicture = bonusPicture;
            this.score = score;
            this.clicheStr = clicheStr;
            this.comment = comment;
            this.trash = trash;
            this.timeAgo = timeAgo;
        }
    }

    public void sortByDescendingVotes() {
        Collections.sort(entries, ClicheAdapter.ClicheVoteComparator.INSTANCE);
        notifyDataSetChanged();
    }

    public void sortByMostRecent() {
        Collections.sort(entries, ClicheAdapter.ClicheDateComparator.INSTANCE);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        final ImageView pictureIv;
        final ImageView bonusPictureIv;
        final TextView scoreTv;
        final TextView clicheStrTv;
        final TextView commentTv;
        final ImageView trashIv;
        final TextView timeAgoTv;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.my_cliche, null);
            pictureIv = (ImageView) convertView.findViewById(R.id.picture);
            bonusPictureIv = (ImageView) convertView.findViewById(R.id.bonus_picture);
            scoreTv = (TextView) convertView.findViewById(R.id.score);
            clicheStrTv = (TextView) convertView.findViewById(R.id.cliche_string);
            commentTv = (TextView) convertView.findViewById(R.id.comment);
            trashIv = (ImageView) convertView.findViewById(R.id.trash);
            timeAgoTv = (TextView) convertView.findViewById(R.id.time_ago);
            viewHolder = new ViewHolder(pictureIv, bonusPictureIv, scoreTv, clicheStrTv, commentTv, trashIv, timeAgoTv);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            pictureIv = viewHolder.picture;
            bonusPictureIv = viewHolder.bonusPicture;
            scoreTv = viewHolder.score;
            clicheStrTv = viewHolder.clicheStr;
            commentTv = viewHolder.comment;
            trashIv = viewHolder.trash;
            timeAgoTv = viewHolder.timeAgo;
        }

        ClicheEntity cliche = entries.get(position);
        final long clicheId = cliche.getId();
        final int startingScore = cliche.getScore();
        int score = startingScore;

        trashIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                removeCliche(clicheId);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

        final String servingUrl = cliche.getServingUrl();
        final String extraPictureServingUrl = cliche.getExtraPictureServingUrl();
        final String clicheStr = cliche.getCliche();
        final String comment = cliche.getComment();
        final String scoreStr = String.valueOf(score);
        final DateTime timeStamp = cliche.getTimeStamp();

        if (Strings.isNullOrEmpty(extraPictureServingUrl)) {
            // Only display servingUrl pic
            String squarePicServingUrl = servingUrl + "=s250-c";
            imageLoader.displayImage(squarePicServingUrl, pictureIv);
            pictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, servingUrl));
            bonusPictureIv.setVisibility(View.GONE);
            // Want to set picture dimensions to 100 dp
            int dimDps = 100;
            // Convert dp to pixels for layout params
            int dimPx = (int) (dimDps * mScale + 0.5f);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dimPx, dimPx);
            pictureIv.setLayoutParams(params);
        } else {
            // Display serving and extra pic
            imageLoader.displayImage(servingUrl, pictureIv);
            imageLoader.displayImage(extraPictureServingUrl, bonusPictureIv);
            pictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, servingUrl));
            bonusPictureIv.setOnClickListener(
                    new ImageFullscreenClickListener((Activity) context, extraPictureServingUrl));
        }

        if (!Strings.isNullOrEmpty(clicheStr)) {
            clicheStrTv.setText(clicheStr);
        }

        if (!Strings.isNullOrEmpty(comment)) {
            commentTv.setText(comment);
        }

        if (!Strings.isNullOrEmpty(scoreStr)) {
            scoreTv.setText(scoreStr);
        }

        if (timeStamp != null) {
            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(timeStamp.getValue());
            if (timeAgoTv == null) {
                Log.e(TAG, "timeAgoTv is null...");
            } else {
                timeAgoTv.setText(timeAgo);
            }
        }

        return convertView;
    }

}
