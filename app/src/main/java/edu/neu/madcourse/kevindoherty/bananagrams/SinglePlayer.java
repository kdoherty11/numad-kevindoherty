package edu.neu.madcourse.kevindoherty.bananagrams;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Board;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Game;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;
import edu.neu.madcourse.kevindoherty.dictionary.Dictionary;

public class SinglePlayer extends Activity implements View.OnClickListener {

    private static final long START_TIME = 1 * 15 * 1000;
    private static final String TIME_PREFS_KEY = "spf_time_bgrams";
    private static final String GAME_STATE_PREFS = "spf_name_gamestate_bgrams";
    private static final String PAUSE_FRAGMENT_TAG = "pause_tag_bgrams";
    private static final String TIME_REMAINING_KEY = "saved_time_remaining_key";
    private static final String GAME_STATE_KEY = "game_state_key";
    private static final String SOUND_ENABLED_KEY = "sound_enabled_key";
    public static final String BOARD_ADAPTER_KEY = "boardAdapter";

    private Game mGame = null;
    private boolean isOver = false;
    private boolean mIsPaused = false;
    private Dictionary mDictionary;

    private GridView mBoardGv;
    private GridView mHolderGv;

    private BoardAdapter mBoardAdapter;
    private TileHolderAdapter mTileHolderAdapter;

    private TextView mPileCountTv;

    protected int mScore = 0;
    private TextView mScoreTv;

    private final SimpleDateFormat mTimeFormat = new SimpleDateFormat("m:ss",
            Locale.getDefault());
    private CountDownTimer mTimer;
    private long mMillisRemaining;
    private TextView mTimerTv;

    private boolean soundsEnabled;
    private ImageView toggleMusicImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mDictionary = new Dictionary(this);

        mBoardGv = (GridView) findViewById(R.id.board);
        mHolderGv = (GridView) findViewById(R.id.tile_holder);

        soundsEnabled = true;

        long startTime = START_TIME;
        boolean gameInit = false;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(TIME_REMAINING_KEY)) {
                startTime = savedInstanceState.getLong(TIME_REMAINING_KEY, -1l);
            }
            if (savedInstanceState.containsKey(GAME_STATE_KEY)) {
                mGame = savedInstanceState.getParcelable(GAME_STATE_KEY);
                gameInit = true;
            }
            if (savedInstanceState.containsKey(SOUND_ENABLED_KEY)) {
                soundsEnabled = savedInstanceState.getBoolean(SOUND_ENABLED_KEY);
            }
            if (savedInstanceState.containsKey(BOARD_ADAPTER_KEY)) {
                mBoardAdapter = savedInstanceState.getParcelable(BOARD_ADAPTER_KEY);
            }
        }

        if (!gameInit) {
            mGame = new Game();
        }

        mGame.toJson();

        toggleMusicImg = (ImageView) findViewById(R.id.sound_toggle);
        if (soundsEnabled) {
            toggleMusicImg.setImageResource(R.drawable.sound);
        } else {
            toggleMusicImg.setImageResource(R.drawable.no_sound);
        }
        if (mBoardAdapter == null) {
            mBoardAdapter = new BoardAdapter(this, mGame.getBoard());
        }
        mTileHolderAdapter = new TileHolderAdapter(this, mGame.getActiveTiles());

        mBoardGv.setAdapter(mBoardAdapter);
        mHolderGv.setAdapter(mTileHolderAdapter);

        initTimer(startTime);

        mPileCountTv = (TextView) findViewById(R.id.pile_count);
        mScoreTv = (TextView) findViewById(R.id.score);

        refreshScore();
        refreshPileSize();

        findViewById(R.id.pause_button).setOnClickListener(this);
        findViewById(R.id.pile).setOnDragListener(new PileDragListener(this));
        toggleMusicImg.setOnClickListener(this);
    }

    private void initTimer(long time) {
        String startTimeStr = mTimeFormat.format(time);

        mTimerTv = (TextView) findViewById(R.id.timerView);
        mTimerTv.setText(startTimeStr);

        mTimer = new CountDownTimer(time, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                mMillisRemaining = millisUntilFinished;
                mTimerTv.setText(mTimeFormat.format(millisUntilFinished));
                if (millisUntilFinished <= 6000) {
                    mTimerTv.setTextColor(getResources().getColor(
                            R.color.red));
                }
            }

            @Override
            public void onFinish() {
                handleGameOver();
            }
        };
        if (!mIsPaused) {
            mTimer.start();
        }
    }

    public void handleGameOver() {
        mTimerTv.setText("0:00");
        isOver = true;
        Intent gameOver = new Intent(this, GameOver.class);
        gameOver.putExtra("score", mScore);
        startActivity(gameOver);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void refreshBoard() {
        mBoardGv.setAdapter(mBoardAdapter);
    }

    public void refreshTileHolder() { mHolderGv.setAdapter(mTileHolderAdapter); }

    public void refreshPileSize() { mPileCountTv.setText(String.valueOf(mGame.getPileSize())); }

    public void refreshScore() {
        UpdateScore myTask = new UpdateScore();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            myTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
        else
            myTask.execute((Void[])null);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.pause_button:
                mIsPaused = true;
                mTimer.cancel();
                mBoardGv.setVisibility(View.GONE);
                mHolderGv.setVisibility(View.GONE);
                Fragment newFragment = new PauseFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.pause_place_holder, newFragment, PAUSE_FRAGMENT_TAG).commit();
                break;
            case R.id.resume:
                mIsPaused = false;
                FragmentManager fm = getFragmentManager();
                fm.beginTransaction().remove(fm.findFragmentByTag(PAUSE_FRAGMENT_TAG)).commit();
                initTimer(mMillisRemaining);
                mBoardGv.setVisibility(View.VISIBLE);
                mHolderGv.setVisibility(View.VISIBLE);
                break;
            case R.id.new_game:
                Intent newGame = new Intent(this, SinglePlayer.class);
                startActivity(newGame);
                break;
            case R.id.main_menu:
                Intent menu = new Intent(this, BananagramsMenu.class);
                startActivity(menu);
                break;
            case R.id.sound_toggle:
                if (soundsEnabled) {
                    Music.pause(this);
                    toggleMusicImg.setImageResource(R.drawable.no_sound);
                    soundsEnabled = false;
                } else {
                    Music.loopMusic(this, R.raw.mario);
                    toggleMusicImg.setImageResource(R.drawable.sound);
                    soundsEnabled = true;
                }
                break;
            default:
                assert(false);
        }
    }

    public Game getGame() { return mGame; }

    public Board getBoard() {
        return mGame.getBoard();
    }

    public boolean isGameOver() { return isOver; }

    public boolean areAllValidWords() {
        return getBoard().areAllValidWords(mDictionary);
    }

    public boolean isAllConnected() { return getBoard().isAllConnected(); }

    public boolean areValidWordsFromSquare(int row, int col) {
        List<List<Tile>> words = getBoard().getWordsFromSquare(row, col);
        if (words.isEmpty()) {
            return false;
        }

        for (List<Tile> word : words) {
            assert(word != null);
            if (!mDictionary.isWord(Board.tilesToString(word))) {
                return false;
            }
        }
        return true;
    }

    public TileHolderAdapter getTileHolderAdapter() { return mTileHolderAdapter; }

    private class UpdateScore extends AsyncTask <Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mGame.calculateScore(mDictionary);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (mScore == 0 || result != mScore) {
                mScore = result;
                mScoreTv.setText(String.valueOf(mScore));
                if (mScore > 99) {
                    mScoreTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                }
                onPostUpdateScore();
            }
        }
    }

    public void onPostUpdateScore() {
        // TODO: Hack
    }

    @Override
    public void onResume() {
        super.onResume();
        if (soundsEnabled) {
            Music.loopMusic(this, R.raw.mario);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Music.pause(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mTimer.cancel();
        SharedPreferences prefs = getSharedPreferences(GAME_STATE_PREFS, Context.MODE_PRIVATE);
        prefs.edit().putLong(TIME_PREFS_KEY, mMillisRemaining).commit();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        if (!mIsPaused) {
            SharedPreferences prefs = getSharedPreferences(GAME_STATE_PREFS, Context.MODE_PRIVATE);
            long timeRemaining = prefs.getLong(TIME_PREFS_KEY, 0l);
            initTimer(timeRemaining);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putLong(TIME_REMAINING_KEY, mMillisRemaining);
        bundle.putParcelable(GAME_STATE_KEY, mGame);
        bundle.putBoolean(SOUND_ENABLED_KEY, soundsEnabled);
        bundle.putParcelable(BOARD_ADAPTER_KEY, mBoardAdapter);
    }

}
