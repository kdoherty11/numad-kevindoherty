package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;

public class FullScreenImage extends Activity {

    private static final ImageLoader imageLoader = ImageLoader.getInstance();

    private static final String TAG = "Full screen cliche image";

    public static final String IMAGE_KEY = "imageUrl";
    public static final String PARENT_KEY = "parentClass";
    public static final String EXTRAS_KEY = "parentClassExtras";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_full_screen_image);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        if (!InternetUtils.checkOnline(this)) {
            return;
        }

        Intent intent = getIntent();
        String servingUrl = intent.getExtras().getString(IMAGE_KEY);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        imageLoader.displayImage(servingUrl, imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent= getIntent();

        //getting the parent class name
        String className = parentIntent.getStringExtra(PARENT_KEY);
        Bundle bundle = null;
        if (parentIntent.getExtras().containsKey(EXTRAS_KEY)) {
            bundle = parentIntent.getBundleExtra(EXTRAS_KEY);
        }
        Intent newIntent = null;

        try {
            newIntent = new Intent(this, Class.forName(className));
            if (bundle != null) {
                newIntent.putExtra(EXTRAS_KEY, bundle);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }
}