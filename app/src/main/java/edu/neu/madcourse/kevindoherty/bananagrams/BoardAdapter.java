package edu.neu.madcourse.kevindoherty.bananagrams;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Board;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/3/14.
 */
public class BoardAdapter extends BaseAdapter implements Parcelable {

    private SinglePlayer mContext;
    private Board mBoard;
    private int mRowOffset = (Board.NUM_ROWS / 2) - 1;
    private int mColOffset = (Board.NUM_COLS / 2) - 1;

   // public static boolean[][] validWords = new boolean[Board.NUM_ROWS][Board.NUM_COLS];

    /** The number of rows to show the user */
    public static final int NUM_ROWS = 12;
    /** The number of columns to show the user */
    public static final int NUM_COLS = 12;

    public BoardAdapter(Context context) {
        this(context, new Board());
    }

    public boolean addToRowOffset(int delta) {
        int newOffset = mRowOffset + delta;
        if (newOffset > 0 && newOffset < Board.NUM_ROWS - NUM_ROWS - 1) {
            mRowOffset = newOffset;
            mContext.refreshBoard();
            return true;
        }
        return false;
    }

    public boolean moveUp() {
        return addToRowOffset(-1);
    }

    public boolean moveDown() {
        return addToRowOffset(1);
    }

    public boolean moveLeft() {
        return addToColOffset(-1);
    }

    public boolean moveRight() {
        return addToColOffset(1);
    }

    public boolean addToColOffset(int delta) {
        int newOffset = mColOffset + delta;
        if (newOffset > 0 && newOffset < Board.NUM_COLS - NUM_COLS - 1) {
            mColOffset = newOffset;
            mContext.refreshBoard();
            return true;
        }
        return false;
    }

    public BoardAdapter(Context context, Board board) {
        this.mContext = (SinglePlayer) context;
        this.mBoard = board;
    }

    @Override
    public int getCount() {
        return NUM_ROWS * NUM_COLS;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    Board getBoard() {
        return mBoard;
    }

    private int getQuadrantRow(int row) {
        if (row < NUM_ROWS / 3) { // row < 4
            return  0;
        } else if (row < NUM_ROWS / 1.5) { // row < 8
            return  1;
        } else { // row => 8
            return 2;
        }
    }

    public int getQuadrantCol(int col) {
        if (col < NUM_COLS / 3) {
            return 0;
        } else if (col < NUM_COLS / 1.5) { // col < 8
            return 1;
        } else { // col => 8
            return 2;
        }
    }

    static class ViewHolder {
        ImageView tileView;
        TextView tileLetter;
    }

    /**
     * Displays each Square in the Boards grid view. This is where the Piece
     * images are set and the Board is checkered.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = (TileView) inflater.inflate(R.layout.tile, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tileView = (ImageView) convertView.findViewById(R.id.tile_background);
            viewHolder.tileLetter = (TextView) convertView.findViewById(R.id.tile_letter);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final int row = (position / NUM_ROWS) + mRowOffset;
        final int col = position % NUM_COLS + mColOffset;

        ImageView tileView = viewHolder.tileView;
        TextView letterView = viewHolder.tileLetter;
        tileView.setAdjustViewBounds(true);

        ((TileView) convertView).setRow(row);
        ((TileView) convertView).setCol(col);
        ((TileView) convertView).setHolderIndex(-1);

        final Tile tile = mBoard.getTile(row, col);
        ((TileView) convertView).setTile(tile);

        if (tile != Tile.MT) {
            letterView.setText(String.valueOf(tile.getLetter()));
            tileView.setTag(tile.getLetter());
            if (!mContext.isGameOver()) {
                convertView.setOnTouchListener(new OnTileTouch(mContext));
            }
        } else {
            tileView.setBackground(mContext.getResources().getDrawable(R.drawable.empty_tile_bg));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quadRow = getQuadrantRow(row - mRowOffset);
                    if (quadRow == 0) {
                        moveUp();
                    } else if (quadRow == 2) {
                        moveDown();
                    }

                    int quadCol = getQuadrantCol(col - mColOffset);
                    if (quadCol == 0) {
                        moveLeft();
                    } else if (quadCol == 2) {
                        moveRight();
                    }
                }
            });
        }

        convertView.setOnDragListener(new BoardDragListener(mContext, mBoard, row, col));


        return convertView;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mRowOffset);
        out.writeInt(mColOffset);
        out.writeParcelable(mBoard, flags);
    }

    public static final Parcelable.Creator<BaseAdapter> CREATOR
            = new Parcelable.Creator<BaseAdapter>() {
        public BaseAdapter createFromParcel(Parcel in) {
            return new BoardAdapter(in);
        }

        public BaseAdapter[] newArray(int size) {
            return new BaseAdapter[size];
        }
    };

    private BoardAdapter(Parcel in) {
        mRowOffset = in.readInt();
        mColOffset = in.readInt();
        mBoard = in.readParcelable(Board.class.getClassLoader());
    }
}
