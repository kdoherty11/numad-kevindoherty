package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.kdoherty.cliche.endpoint.cliche.model.JsonMap;

import java.io.IOException;
import java.math.BigDecimal;

import edu.neu.madcourse.kevindoherty.R;

public class CommercialClicheHome extends Activity implements View.OnClickListener {

    private static final String TAG = "Commercial Cliche Home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.activity_commercial_cliche_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setScore();
        findViewById(R.id.cc_play_button).setOnClickListener(this);
        findViewById(R.id.cc_see_my_pictures).setOnClickListener(this);
        findViewById(R.id.cc_see_all_pictures).setOnClickListener(this);
        findViewById(R.id.cc_cliche_acknowledgements).setOnClickListener(this);

        SharedPreferences prefs = getSharedPreferences(AddDescription.PREFS_NAME, Context.MODE_PRIVATE);
        if (!prefs.contains("submittedCliche")) {
            findViewById(R.id.cc_see_my_pictures).setVisibility(View.GONE);
        }
    }

    private void setScore() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    JsonMap json = Endpoints.CLICHE_API.getScore(ClicheUtils.getUserId(CommercialClicheHome.this)).execute();
                    BigDecimal score = (BigDecimal) json.get("score");
                    Log.d(TAG, "Server returned score of " + score);
                    return score.toString();
                } catch (IOException e) {
                    Log.e(TAG, "Error getting score from server " + e.getMessage());
                    return "0";
                }
            }

            @Override
            protected void onPostExecute(String score) {
                Log.d(TAG, "Updated cliche score to " + score);
                ((TextView) findViewById(R.id.cliche_score)).setText("Your Cliche Score: " + score);
            }
        }.execute();

    }

    @Override
    public void onResume() {
        super.onResume();
        setScore();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.cc_play_button:
                Intent intent = new Intent(getApplicationContext(), SelectCliche.class);
                startActivity(intent);
                break;
            case R.id.cc_see_all_pictures:
                Intent myPics = new Intent(getApplicationContext(), AllPictures.class);
                startActivity(myPics);
                break;
            case R.id.cc_see_my_pictures:
                Intent allPics = new Intent(getApplicationContext(), MyPictures.class);
                startActivity(allPics);
                break;
            case R.id.cc_cliche_acknowledgements:
                Intent ack = new Intent(getApplicationContext(), ClicheAcknowledgements.class);
                startActivity(ack);
        }


    }
}
