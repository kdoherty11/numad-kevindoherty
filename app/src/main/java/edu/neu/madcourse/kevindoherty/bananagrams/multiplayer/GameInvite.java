package edu.neu.madcourse.kevindoherty.bananagrams.multiplayer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.kdoherty.gcm.endpoint.games.model.Game;

import java.io.IOException;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.communication.CommunicationsMenu;

public class GameInvite extends Activity implements View.OnClickListener {

    String senderRegId;

    private static final String TAG = "Game Invite";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_invite);
        findViewById(R.id.accept).setOnClickListener(this);
        findViewById(R.id.decline).setOnClickListener(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey("senderRegId")) {
            senderRegId = extras.getString("senderRegId");
        } else if (Strings.isNullOrEmpty(senderRegId)) {
            Log.w(TAG, "Unable to get senderRegId from extras: " + extras);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game_invite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendGcm(final boolean accept, final String gameId) {
        if (Strings.isNullOrEmpty(senderRegId)) {
            Toast.makeText(this, "No sender reg id", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String name = prefs.getString("name", "Unknown");

        final String message;
        if (accept) {
            message = name + " has accepted your invitation to play bananagrams!";
        } else {
            message = name + " has declined your invitation to play bananagrams...";
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(GameInvite.this)) {
                        Endpoints.MESSAGING.sendGameInviteResponse(message, senderRegId, GcmUtils.getRegistrationId(GameInvite.this), accept, gameId).execute();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private void createGameOnServer() {
        new AsyncTask<Void, Void, Game>() {

            @Override
            protected Game doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(GameInvite.this)) {
                        return Endpoints.GAMES.createGame(GcmUtils.getRegistrationId(GameInvite.this), senderRegId).execute();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Game was not created on the server " + e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Game game) {
                if (game == null) {
                    // No internet
                    return;
                }
                final String gameId = game.getId();
                sendGcm(true, gameId);
                Intent twoPlayer = new Intent(GameInvite.this, TwoPlayer.class);
                twoPlayer.putExtra("oppRegId", senderRegId);
                twoPlayer.putExtra("gameId", gameId);
                startActivity(twoPlayer);
            }
        }.execute();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.accept:
                createGameOnServer();
                break;
            case R.id.decline:
                sendGcm(false, "");
                Intent menu = new Intent(this, CommunicationsMenu.class);
                startActivity(menu);
                break;
        }
    }
}
