package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.util.UUID;

/**
 * Created by kdoherty on 11/15/14.
 */
public class ClicheUtils {

    private static final String PREFS_KEY = "cliche";
    private static final String USER_ID_KEY = "userId";
    private static final String TAG = "Cliche Utils";

    private ClicheUtils() {
        // Hide constructor
    }

    public static String getUserId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String userId = prefs.getString(USER_ID_KEY, "");

        if (Strings.isNullOrEmpty(userId)) {
            Log.i(TAG, "Generating new userId");
            userId = generateUserId(context);
            prefs.edit().putString(USER_ID_KEY, userId).apply();
        } else {
            Log.d(TAG, "Using userId from Prefs");
        }

        Log.d(TAG, "Returning userId of " + userId);

        return userId;

    }

    private static String generateUserId(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm == null) {
            return "";
        }

        String userId = tm.getDeviceId();

        if (Strings.isNullOrEmpty(userId)) {
            // For tablets
            userId = UUID.randomUUID().toString();
        }

        return userId;
    }
}
