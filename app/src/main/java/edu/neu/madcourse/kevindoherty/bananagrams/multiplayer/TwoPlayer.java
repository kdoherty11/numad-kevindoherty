package edu.neu.madcourse.kevindoherty.bananagrams.multiplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.io.IOException;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.SinglePlayer;

public class TwoPlayer extends SinglePlayer {

    /**
     * Tag used on log messages.
     */
    private static final String TAG = "Two Player Banannagrams";

    final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "BROADCAST RECEIVED IN TWO PLAYER");

            Bundle extras = intent.getExtras();

            if (extras != null && extras.containsKey("score")) {
                int score = Integer.valueOf(extras.getString("score"));
                updateOpponentScore(score);
                abortBroadcast();
            }


        }
    };

    private String oppRegId;
    private String gameId;
    private TextView oppScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.containsKey("oppRegId")) {
                oppRegId = extras.getString("oppRegId");
            }
            if (extras.containsKey("gameId")) {
                gameId = extras.getString("gameId");
                Log.i(TAG, "GAMEID: " + gameId);
            } else {
                Log.w(TAG, "Starting TwoPlayer without a game ID");
            }
        }
    }

    private void updateOpponentScore(int score) {

        Log.i(TAG, "Opponent's score updated to " + score);
        if (oppScore == null) {
            oppScore = new TextView(this);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            oppScore.setLayoutParams(params);
            oppScore.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            ((RelativeLayout) findViewById(R.id.layout)).addView(oppScore);
        }
        oppScore.setText("Opponent's Score: " + score);
    }

    @Override
    public void handleGameOver() {
        notifyServerOfCompletion();
        sendScoreToLeaderboard();
        super.handleGameOver();
    }

    private void notifyServerOfCompletion() {
        if (Strings.isNullOrEmpty(gameId)) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(TwoPlayer.this, "You must be online while playing this game.")) {
                        Endpoints.GAMES.updateScore(gameId, GcmUtils.getRegistrationId(TwoPlayer.this), mScore).execute();
                        Log.d(TAG, "Final score sent to server");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            if (InternetUtils.checkOnline(TwoPlayer.this, "You must be online while playing this game.")) {
                                String regId = GcmUtils.getRegistrationId(TwoPlayer.this);
                                Endpoints.GAMES.isFinished(gameId, regId).execute();
                                Log.i(TAG, "Finished and sent to server. Sent with regId " + regId);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }
        }.execute();
    }

    @Override
    public void onPostUpdateScore() {

        if (Strings.isNullOrEmpty(gameId)) {
            Log.w(TAG, "GameId is null");
            return;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(TwoPlayer.this, "You must be online while playing this game.")) {
                        Endpoints.GAMES.updateScore(gameId, GcmUtils.getRegistrationId(TwoPlayer.this), mScore).execute();
                        Log.d(TAG, "Score sent to server");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();

    }

    private void sendScoreToLeaderboard() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String name = prefs.getString("name", "Unknown");

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(TwoPlayer.this, "Please connect to the internet " +
                            "if you would like to submit your score to the leaderboard.")) {
                        Endpoints.LEADERBOARD.submit(name, mScore).execute();
                        Log.d(TAG, "Score submitted to leaderboard");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "Unregistered receiver");
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter("edu.neu.madcourse.kevindoherty");
        filter.addAction("edu.neu.madcourse.kevindoherty.GOT_PUSH");
        filter.addAction("com.google.android.c2dm.intent.RECEIVE");
        filter.addAction("GCM_RECEIVED_ACTION");

        filter.setPriority(100);
        registerReceiver(mBroadcastReceiver, filter);
        Log.d(TAG, "Receiver registered");
        super.onResume();
    }
}
