//package edu.neu.madcourse.kevindoherty.finalprojectstart;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//
//import java.util.List;
//
//import edu.neu.madcourse.kevindoherty.R;
//
///**
// * Created by kdoherty on 11/30/14.
// */
//public class SelectClicheAdapter extends ArrayAdapter<String> {
//
//    private static final String TAG = "Select Cliche Adapter";
//    private static final String PREFS_NAME = "CommercialClichePrefs";
//    private static final String ON_CLICHE = "Attempting this cliche num:";
//
//
//    List<String> cliches;
//    Context context;
//
//    public SelectClicheAdapter(Context context, int resource, List<String> cliches) {
//        super(context, resource, cliches);
//        this.cliches = cliches;
//        this.context = context;
//    }
//
//    static class ViewHolder {
//        Button cliche;
//        Button swap;
//
//
//        public ViewHolder(Button cliche, Button swap) {
//            this.cliche = cliche;
//            this.swap = swap;
//        }
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        ViewHolder viewHolder;
//        final Button clicheBtn;
//        final Button swapBtn;
//
//        if (convertView == null) {
//            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = inflater.inflate(R.layout.clicheStr, null);
//            clicheBtn = (Button) convertView.findViewById(R.id.clicheBtn);
//            swapBtn = (Button) convertView.findViewById(R.id.swapBtn);
//            viewHolder = new ViewHolder(clicheBtn, swapBtn);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//            clicheBtn = viewHolder.cliche;
//            swapBtn = viewHolder.swap;
//        }
//
//        String clicheStr = cliches.get(position);
//
//        clicheBtn.setText(clicheStr);
//
//        clicheBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // TODO
//                // take picture and stuff
//            }
//        });
//
//        swapBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // TODO
//                // swap out cliche for a new one
//
//                notifyDataSetChanged();
//            }
//        });
//
//        return convertView;
//    }
//}
