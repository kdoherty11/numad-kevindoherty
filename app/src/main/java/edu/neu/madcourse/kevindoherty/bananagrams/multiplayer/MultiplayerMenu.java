package edu.neu.madcourse.kevindoherty.bananagrams.multiplayer;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.api.client.util.Strings;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.SinglePlayer;
import edu.neu.madcourse.kevindoherty.communication.Leaderboard;
import edu.neu.madcourse.kevindoherty.communication.Register;
import edu.neu.madcourse.kevindoherty.communication.RegisteredPlayers;

public class MultiplayerMenu extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplayer_menu);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        findViewById(R.id.single_player).setOnClickListener(this);
        findViewById(R.id.multiplayer).setOnClickListener(this);
        findViewById(R.id.multiplayer_ack_button).setOnClickListener(this);
        findViewById(R.id.leaderboard_button).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.multiplayer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.multiplayer:
                if (PreferenceManager.getDefaultSharedPreferences(this).contains("name") &&
                        !Strings.isNullOrEmpty(GcmUtils.getRegistrationId(this))) {
                    Intent activePlayers = new Intent(this, RegisteredPlayers.class);
                    startActivity(activePlayers);
                } else {
                    Intent reg = new Intent(this, Register.class);
                    startActivity(reg);
                }
                break;
            case R.id.leaderboard_button:
                Intent lb = new Intent(this, Leaderboard.class);
                startActivity(lb);
                break;
            case R.id.single_player:
                Intent singlePlayer = new Intent(this, SinglePlayer.class);
                startActivity(singlePlayer);
                break;

            case R.id.multiplayer_ack_button:
                startActivity(new Intent(this, MultiplayerAck.class));
                break;
        }
    }
}
