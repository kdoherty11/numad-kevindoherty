package edu.neu.madcourse.kevindoherty.bananagrams;

import android.view.DragEvent;
import android.view.View;

import edu.neu.madcourse.kevindoherty.bananagrams.model.Board;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/6/14.
 */
public class HolderDragListener implements View.OnDragListener {

    private TileHolderAdapter holder;
    private SinglePlayer context;
    private int position;

    public HolderDragListener(TileHolderAdapter holder, SinglePlayer context, int position) {
        this.holder = holder;
        this.context = context;
        this.position = position;
    }

    @Override
    public boolean onDrag(View v, DragEvent dragEvent) {
        int action = dragEvent.getAction();
        TileView view = (TileView) dragEvent.getLocalState();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                final View droppedView = (View) dragEvent.getLocalState();
                if (droppedView != null) {
                    droppedView.post(new Runnable() {
                        @Override
                        public void run() {
                            droppedView.setVisibility(View.VISIBLE);
                        }
                    });
                }
                break;
            case DragEvent.ACTION_DROP:
                if (view == null) {
                    break;
                }
                int holderIndex = view.getHolderIndex();
                if (holderIndex != -1) {
                    // Holder to holder
                    holder.swap(holderIndex, position);
                } else {
                    // From board to holder
                    // Remove tile from board
                    Board board = ((SinglePlayer) context).getBoard();
                    Tile removed = board.removeTile(view.getRow(), view.getCol());

                    // Add tile to board from holder and remove it from holder
                    Tile holderRemoved = holder.remove(position);
                    if (holderRemoved != Tile.MT) {
                        board.placeTile(view.getRow(), view.getCol(), holderRemoved);
                    }

                    holder.addAtIndex(position, removed);
                    context.refreshScore();
                    context.refreshBoard();
                }
                holder.notifyDataSetChanged();
                context.refreshTileHolder();
                break;
            default:
                return false;
        }
        return true;
    }
}
