package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import edu.neu.madcourse.kevindoherty.R;

public class SelectCliche extends Activity implements View.OnClickListener {

    public static final int REQUEST_TAKE_PHOTO_1 = 1;
    public static final int REQUEST_TAKE_PHOTO_2 = 2;
    public static final int REQUEST_ADD_DESCRIPTION = 3;
    private static final String TAG = "Select Cliche";
    public static final String PREFS_NAME = "CommercialClichePrefs";
    public static final String CLICHE_NUM = "CLICHE_IN_SLOT_";
    public static final String CLICHE_MAX = "Next_cliche_num";
    public static final String ON_CLICHE = "Attempting this cliche num:";
    public static final String ACTIVITY_RESULT_DESCRIPTION = "Users_description";

    public static final String PHOTO_PATH_KEY = "firstphotopath";
    public static final String BONUS_PHOTO_KEY = "secondphotopath";

    protected String mCurrentPhotoPath = "";
    protected String mBonusPhotoPath = "";

    private int[] bingocard = new int[6];
    private List<String> database = Arrays.asList("Women in dresses eating yogurt",
            "'Innovation'", "Splashing water on face",
            "Talking baby",
            "'BRAWMMM' sound in movie trailer",
            "Symptoms of medication worse than disease",
            "'Us vs. them' tech ad",
            "Talking food",
            "Car driving around cliffs",
            "Commercial has nothing to do with product",
            "Clumsy kids!",
            "Extremely thin device",
            "Fast food made to look healthy",
            "50% off",
            "Playing with dogs",
            "Unrealistically diverse group of friends",
            "Laughing",
            "Black and white",
            "Details at 11...",
            "Sex!");
    List<Integer> numsInDeck = new ArrayList<Integer>();

    Button cliche1;
    Button cliche2;
    Button cliche3;
    Button cliche4;
    Button cliche5;
    Button cliche6;
    ImageView swap1;
    ImageView swap2;
    ImageView swap3;
    ImageView swap4;
    ImageView swap5;
    ImageView swap6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_cliche);
        getActionBar().hide();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey("displayToast")) {
            if (getIntent().getExtras().getBoolean("displayToast")) {
                Toast.makeText(this, "Cliche Posted!", Toast.LENGTH_SHORT).show();
            }
        }

        cliche1 = (Button) findViewById(R.id.cliche_choice_1);
        cliche1.setOnClickListener(this);
        cliche2 = (Button) findViewById(R.id.cliche_choice_2);
        cliche2.setOnClickListener(this);
        cliche3 = (Button) findViewById(R.id.cliche_choice_3);
        cliche3.setOnClickListener(this);
        cliche4 = (Button) findViewById(R.id.cliche_choice_4);
        cliche4.setOnClickListener(this);
        cliche5 = (Button) findViewById(R.id.cliche_choice_5);
        cliche5.setOnClickListener(this);
        cliche6 = (Button) findViewById(R.id.cliche_choice_6);
        cliche6.setOnClickListener(this);

        swap1 = (ImageView) findViewById(R.id.swap_1);
        swap1.setOnClickListener(this);
        swap2 = (ImageView) findViewById(R.id.swap_2);
        swap2.setOnClickListener(this);
        swap3 = (ImageView) findViewById(R.id.swap_3);
        swap3.setOnClickListener(this);
        swap4 = (ImageView) findViewById(R.id.swap_4);
        swap4.setOnClickListener(this);
        swap5 = (ImageView) findViewById(R.id.swap_5);
        swap5.setOnClickListener(this);
        swap6 = (ImageView) findViewById(R.id.swap_6);
        swap6.setOnClickListener(this);

        findViewById(R.id.return_to_menu_button).setOnClickListener(this);

        initializeCliches();

    }

    private void initializeCliches() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        int max = 0;
        for (int i=0; i<6; i++) {
            bingocard[i] = settings.getInt(CLICHE_NUM+i, i);
            numsInDeck.add(bingocard[i]);
            if (bingocard[i] > max) {
                max = bingocard[i];
            }
        }
        cliche1.setText(database.get(bingocard[0]));
        cliche2.setText(database.get(bingocard[1]));
        cliche3.setText(database.get(bingocard[2]));
        cliche4.setText(database.get(bingocard[3]));
        cliche5.setText(database.get(bingocard[4]));
        cliche6.setText(database.get(bingocard[5]));

        if (max >= database.size()) {
            max -= database.size();
        }
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(CLICHE_MAX, max+1);
        editor.apply();

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        switch (id) {
            case R.id.return_to_menu_button:
                Intent menu = new Intent(getApplicationContext(), CommercialClicheHome.class);
                startActivity(menu);
                break;
            case R.id.cliche_choice_1:
                editor.putString(ON_CLICHE, database.get(bingocard[0]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.cliche_choice_2:
                editor.putString(ON_CLICHE, database.get(bingocard[1]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.cliche_choice_3:
                editor.putString(ON_CLICHE, database.get(bingocard[2]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.cliche_choice_4:
                editor.putString(ON_CLICHE, database.get(bingocard[3]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.cliche_choice_5:
                editor.putString(ON_CLICHE, database.get(bingocard[4]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.cliche_choice_6:
                editor.putString(ON_CLICHE, database.get(bingocard[5]));
                editor.apply();
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_1);
                break;
            case R.id.swap_1:
                cliche1.setText(database.get(swap(0)));
                break;
            case R.id.swap_2:
                cliche2.setText(database.get(swap(1)));
                break;
            case R.id.swap_3:
                cliche3.setText(database.get(swap(2)));
                break;
            case R.id.swap_4:
                cliche4.setText(database.get(swap(3)));
                break;
            case R.id.swap_5:
                cliche5.setText(database.get(swap(4)));
                break;
            case R.id.swap_6:
                cliche6.setText(database.get(swap(5)));
                break;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "On Activity Result");
        boolean isBonus = requestCode == REQUEST_TAKE_PHOTO_2;
        if (requestCode == REQUEST_TAKE_PHOTO_1 && resultCode == RESULT_OK) {
            Log.d(TAG, "Successfully completed taking first photo");
            addPhotoToGallery(mCurrentPhotoPath);
            Intent extraPhotoQuestion = new Intent(getApplicationContext(), ExtraPhotoQuestion.class);
            extraPhotoQuestion.putExtra(PHOTO_PATH_KEY, mCurrentPhotoPath);
            extraPhotoQuestion.putExtra(BONUS_PHOTO_KEY, mBonusPhotoPath);
            startActivity(extraPhotoQuestion);
        }

        if (isBonus && resultCode == RESULT_OK) {
            Log.d(TAG, "Successfully completed taking bonus photo");
            addPhotoToGallery(mBonusPhotoPath);
            Intent intent = new Intent(SelectCliche.this, AddDescriptionTwoPics.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(AddDescription.FIRST_IMAGE_PATH,  mCurrentPhotoPath);
            intent.putExtra(AddDescription.BONUS_IMAGE_PATH, mBonusPhotoPath);
            startActivityForResult(intent, REQUEST_ADD_DESCRIPTION);
            finish();
            // Start add description activity for 2 pictures
        } else if (requestCode == REQUEST_ADD_DESCRIPTION) {
            Toast.makeText(this, "Cliche Posted!", Toast.LENGTH_SHORT).show();
        }
    }

    public int swap(int i) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        Integer next = settings.getInt(CLICHE_MAX, 0);
        while (Arrays.asList(bingocard).contains(next)) {
            next++;
            if (next >= database.size()) {
                next -= database.size();
            }
        }
        while (numsInDeck.contains(next)) {
            next++;
            if (next >= database.size()) {
                next -= database.size();
            }
        }
        if (next >= database.size()) {
            next -= database.size();
        }
        Integer current = bingocard[i];
        numsInDeck.remove(current);
        numsInDeck.add(next);
        bingocard[i] = next;
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(CLICHE_NUM+i, next);
        editor.putInt(CLICHE_MAX, next+1);
        editor.apply();
        return next;
    }

    protected void dispatchTakePictureIntent(int photoNum) {

        boolean isBonus = photoNum == REQUEST_TAKE_PHOTO_2;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(isBonus);
                Log.i(TAG, "Successfully created picture file");
            } catch (IOException ex) {
                Log.e(TAG, "Error occured while creating picture file: " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, photoNum);
            }
        }
    }

    protected File createImageFile(boolean isBonus) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if (isBonus) {
            mBonusPhotoPath = image.getAbsolutePath();
        } else {
            mCurrentPhotoPath = image.getAbsolutePath();
        }
        return image;
    }

    protected void addPhotoToGallery(String filePath) {
        if (Strings.isNullOrEmpty(filePath)) {
            Log.w(TAG, "filePath is null in addPhotoToGallery");
            return;
        }
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
