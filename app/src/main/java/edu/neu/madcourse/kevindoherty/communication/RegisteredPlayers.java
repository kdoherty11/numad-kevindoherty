package edu.neu.madcourse.kevindoherty.communication;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.kdoherty.gcm.endpoint.registration.model.RegistrationRecord;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.Endpoints;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmUtils;

public class RegisteredPlayers extends Activity {

    private static final String TAG = "Registered players";

    private ListView lvPlayerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_registered_players);
        GcmUtils.register(this);
        lvPlayerList = (ListView) findViewById(R.id.playerList);
        showRegisteredDevices();

    }

    private void showRegisteredDevices() {
        new AsyncTask<Void, Void, List<RegistrationRecord>>() {
            @Override
            protected List<RegistrationRecord> doInBackground(Void... voids) {
                try {
                    if (InternetUtils.checkOnline(RegisteredPlayers.this)) {
                        return Endpoints.REG.listDevices(50).execute().getItems();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<RegistrationRecord> listDevices) {
                if (listDevices == null) {
                    return;
                }
                if (listDevices.isEmpty()) {
                    Toast.makeText(RegisteredPlayers.this, "No other players found", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (RegistrationRecord regEntry : listDevices) {
                    Log.i(TAG, "regEntryName: " + regEntry.getName());
                }
                PlayerListAdapater adapater = new PlayerListAdapater(RegisteredPlayers.this, 0, listDevices);
                lvPlayerList.setAdapter(adapater);
                lvPlayerList.setOnItemClickListener(adapater);

            }
        }.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registered_players, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
