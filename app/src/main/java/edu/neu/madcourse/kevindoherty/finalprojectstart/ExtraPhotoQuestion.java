package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.kevindoherty.R;

public class ExtraPhotoQuestion extends SelectCliche implements View.OnClickListener {

    String mCurrentPhotoPath;
    String mBonusPhotoPath;
    private static final String TAG = "Extra photo question";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_photo_question);

        Bundle extras = getIntent().getExtras();

        mCurrentPhotoPath = extras.getString(SelectCliche.PHOTO_PATH_KEY);
        mBonusPhotoPath = extras.getString(SelectCliche.BONUS_PHOTO_KEY);

        findViewById(R.id.yes).setOnClickListener(this);
        findViewById(R.id.no).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "On Activity Result");

        if (resultCode == RESULT_OK) {
            Log.d(TAG, "Successfully completed taking bonus photo");
            addPhotoToGallery(super.mBonusPhotoPath);
            Intent intent = new Intent(this, AddDescriptionTwoPics.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(AddDescription.FIRST_IMAGE_PATH,  mCurrentPhotoPath);
            intent.putExtra(AddDescription.BONUS_IMAGE_PATH, super.mBonusPhotoPath);
            startActivityForResult(intent, REQUEST_ADD_DESCRIPTION);
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.extra_photo_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.yes:
                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO_2);
                break;
            case R.id.no:
                Intent intent = new Intent(getApplicationContext(), AddDescription.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.putExtra(AddDescription.FIRST_IMAGE_PATH, mCurrentPhotoPath);
                intent.putExtra(AddDescription.BONUS_IMAGE_PATH, mBonusPhotoPath);
                startActivityForResult(intent, SelectCliche.REQUEST_ADD_DESCRIPTION);
                break;
        }
    }
}
