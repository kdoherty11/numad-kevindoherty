package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import edu.neu.madcourse.kevindoherty.R;

public class AddDescriptionTwoPics extends AddDescription {

    private static final String PREFS_NAME = "CommercialClichePrefs";
    private static final String CURRENT_IMAGE_PATH = "Path_to_image";
    public static final String BONUS_IMAGE_PATH = "BonusImagePath";
    private static final String TAG = "Add Cliche Description Two Pics";

    private String mCurrentPhotoPath1 = null;
    private String mCurrentPhotoPath2 = null;
    private ImageView photo1;
    private ImageView photo2;

    private EditText description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_add_description_two_pics);
        photo1 = (ImageView) findViewById(R.id.add_descr_two_pictures_picture1);
        photo2 = (ImageView) findViewById(R.id.add_descr_two_pictures_picture2);
        findViewById(R.id.add_descr_publish_button).setOnClickListener(this);
        findViewById(R.id.add_descr_cancel_button).setOnClickListener(this);

        description = (EditText) findViewById(R.id.description);

        description.setOnKeyListener(new View.OnKeyListener() {
            /**
             * This listens for the user to press the enter button on
             * the keyboard and then hides the virtual keyboard
             */
            public boolean onKey(View arg0, int arg1, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (arg1 == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(description.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        mCurrentPhotoPath1 = getIntent().getStringExtra(CURRENT_IMAGE_PATH);
        mCurrentPhotoPath2 = getIntent().getStringExtra(BONUS_IMAGE_PATH);

        Log.d(TAG, "mCurrentPhotoPath1=" + mCurrentPhotoPath1);
        Log.d(TAG, "mCurrentPhotoPath2=" + mCurrentPhotoPath2);

        photo1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Remove it here unless you want to get this callback for EVERY
                //layout pass, which can get you into infinite loops if you ever
                //modify the layout from within this method.
                photo1.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                ImageUtils.setPic(mCurrentPhotoPath1, photo1);
                Log.i(TAG, "getPic called");
                //Now you can get the width and height from content
            }
        });
        photo2.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Remove it here unless you want to get this callback for EVERY
                //layout pass, which can get you into infinite loops if you ever
                //modify the layout from within this method.
                photo2.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                ImageUtils.setPic(mCurrentPhotoPath2, photo2);
                Log.i(TAG, "getPic called");
                //Now you can get the width and height from content
            }
        });

        Log.i(TAG, "onCreate over");
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch(id) {
            case R.id.add_descr_publish_button:
                publish(String.valueOf(description.getText()));
                break;
            case R.id.add_descr_cancel_button:
                areYouSure();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_description_two_pics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void areYouSure() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Intent playScreen = new Intent(getApplicationContext(), SelectCliche.class);
                        startActivity(playScreen);
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(LEAVE_PAGE_WARNING).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
