package edu.neu.madcourse.kevindoherty.bananagrams.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.kevindoherty.dictionary.Dictionary;

/**
 * Created by kdoherty on 10/2/14.
 */
public final class Game implements Parcelable {

    private static final String TAG = "Game";

    public static final int NUM_STARTING_TILES = 21;
    public static final int NUM_EXCHANGE_TILES = 3;

    private final List<Tile> pile = new ArrayList<>();

    public final List<Tile> activeTiles = new ArrayList<>();

    public Board board = new Board();

    private static final Map<Tile, Integer> letterCounts = new HashMap<>();

    static {
        letterCounts.put(Tile.A, 13);
        letterCounts.put(Tile.B, 3);
        letterCounts.put(Tile.C, 3);
        letterCounts.put(Tile.D, 6);
        letterCounts.put(Tile.E, 18);
        letterCounts.put(Tile.F, 3);
        letterCounts.put(Tile.G, 4);
        letterCounts.put(Tile.H, 3);
        letterCounts.put(Tile.I, 12);
        letterCounts.put(Tile.J, 2);
        letterCounts.put(Tile.K, 2);
        letterCounts.put(Tile.L, 5);
        letterCounts.put(Tile.M, 3);
        letterCounts.put(Tile.N, 8);
        letterCounts.put(Tile.O, 11);
        letterCounts.put(Tile.P, 3);
        letterCounts.put(Tile.Q, 2);
        letterCounts.put(Tile.R, 9);
        letterCounts.put(Tile.S, 6);
        letterCounts.put(Tile.T, 9);
        letterCounts.put(Tile.U, 6);
        letterCounts.put(Tile.V, 3);
        letterCounts.put(Tile.W, 3);
        letterCounts.put(Tile.X, 2);
        letterCounts.put(Tile.Y, 3);
        letterCounts.put(Tile.Z, 2);
    }

    public Game() {
        initPile();
        Collections.shuffle(pile);
        deal(NUM_STARTING_TILES);
    }

    public List<Tile> getActiveTiles() {
        return activeTiles;
    }

    private void initPile() {
        for (Map.Entry<Tile, Integer> entry : letterCounts.entrySet()) {
            Tile tile = entry.getKey();
            int count = entry.getValue();
            for (int i = 0; i < count; i++) {
                pile.add(tile);
            }
        }
    }

    public void deal(int num) {
        for (int i = 0; i < num; i++) {
            activeTiles.add(pile.remove(0));
        }
    }

    public List<Tile> dumpTile(Tile tile) {
        List<Tile> newTiles = new ArrayList<>();
        if (pile.size() < NUM_EXCHANGE_TILES) {
            return newTiles;
        }
        for (int i = 0; i < NUM_EXCHANGE_TILES; i++) {
            newTiles.add(takeTile());
        }
        pile.add(tile);
        Collections.shuffle(pile);
        return newTiles;
    }

    public Tile takeTile() {
        if (!pile.isEmpty())
            return pile.remove(0);
        return Tile.MT;
    }

    public Board getBoard() {
        return board;
    }

    public int getPileSize() {
        return pile.size();
    }

    public int calculateScore(Dictionary dictionary) {
        return board.calculateScore(dictionary);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(board, i);
        System.out.println("Writing board: \n" + board);
        parcel.writeTypedList(pile);
    }

    public static final Parcelable.Creator<Game> CREATOR
            = new Parcelable.Creator<Game>() {
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        public Game[] newArray(int size) {
            return new Game[size];
        }
    };
    private Game(Parcel in) {
        board = in.readParcelable(Board.class.getClassLoader());
        System.out.println("Read board: \n" + board);
        in.readTypedList(pile, Tile.CREATOR);
    }

    public String toJson() {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(this);
        Log.i(TAG, json);
        Log.i(TAG, "" + json.length());
        return json;
    }

    public static Game fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, Game.class);
    }
}
