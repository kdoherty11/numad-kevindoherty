package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.kevindoherty.R;

public class PlayStoreDescription extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_store_description);
    }

    public void startGame(View view) {
        Intent cc = new Intent(this, CommercialClicheHome.class);
        startActivity(cc);
    }
}
