package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.kevindoherty.InternetUtils;

public class MyPictures extends AllPictures {

    private static final String TAG = "My Cliches";
    private MyClicheAdapter adapter;
    private boolean tabsAdded = false;
    private boolean sortedByVote = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title.setText("My Cliches");
    }

    @Override
    protected void displayPictures(final boolean first) {
        if (!InternetUtils.checkOnline(this)) {
            progressBar.setVisibility(View.GONE);
            return;
        }

        new AsyncTask<Void, Void, List<ClicheEntity>>() {
            @Override
            protected List<ClicheEntity> doInBackground(Void... voids) {
                try {
                    return Endpoints.CLICHE_API.getClichesByUserId(ClicheUtils.getUserId(MyPictures.this))
                            .execute().getItems();
                } catch (IOException e) {
                    Log.e(TAG, "Error getting pictures by id: " + e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<ClicheEntity> listEntries) {
                progressBar.setVisibility(View.GONE);
                if (listEntries == null || listEntries.isEmpty()) {
                    Toast.makeText(MyPictures.this, "You have not submitted any cliches!", Toast.LENGTH_SHORT).show();
                    return;
                }
                adapter = new MyClicheAdapter(MyPictures.this, 0, listEntries);

                if (sortedByVote) {
                    adapter.sortByDescendingVotes();
                }

                lvPictures.setAdapter(adapter);

                if (!tabsAdded) {

                    final ActionBar actionBar = getActionBar();
                    // Specify that tabs should be displayed in the action bar.
                    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

                    // Create a tab listener that is called when the user changes tabs.
                    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
                        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                            if (tab.getText().toString().equals("Popular")) {
                                adapter.sortByDescendingVotes();
                                sortedByVote = true;
                            } else {
                                adapter.sortByMostRecent();
                            }
                        }

                        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                            Log.v(TAG, "Tab " + tab.getText() + " unselected");
                            // hide the given tab
                        }

                        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                            // probably ignore this event
                            Log.v(TAG, "Tab " + tab.getText() + " reselected");
                        }
                    };

                    actionBar.addTab(
                            actionBar.newTab()
                                    .setText("Recent")
                                    .setTabListener(tabListener));

                    actionBar.addTab(
                            actionBar.newTab()
                                    .setText("Popular")
                                    .setTabListener(tabListener));
                    tabsAdded = true;
                }

//                lvPictures.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        ClicheEntity clicheEntity = (ClicheEntity) adapterView.getItemAtPosition(i);
//                        Intent clicheActivity = new Intent(getApplicationContext(), ClicheActivity.class);
//                        clicheActivity.putExtra(ClicheActivity.CLICHE_KEY, new Gson().toJson(clicheEntity));
//                        clicheActivity.putExtra(ClicheActivity.PARENT_KEY, getClass().getName());
//                        List<String> votedOn = clicheEntity.getVotedOn();
//                        ArrayList<String> votedOnArrayList;
//                        if (votedOn == null) {
//                            votedOnArrayList = new ArrayList<>();
//                        } else {
//                            votedOnArrayList = new ArrayList<>(votedOn);
//                        }
//                        clicheActivity.putStringArrayListExtra("votedOn", votedOnArrayList);
//                        int score = Integer.valueOf(((TextView) view.findViewById(R.id.score)).getText().toString());
//                        clicheActivity.putExtra("score", score);
//                        startActivity(clicheActivity);
//                    }
//                });

                title.setVisibility(View.VISIBLE);
                lvPictures.setVisibility(View.VISIBLE);
            }
        }.execute();
    }

}
