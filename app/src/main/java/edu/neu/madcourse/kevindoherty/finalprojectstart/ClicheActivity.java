package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;

public class ClicheActivity extends Activity {

    private static final String TAG = "Cliche Activity";
    private static final ImageLoader imageLoader = ImageLoader.getInstance();

    public static final String CLICHE_KEY = "clicheEntity";
    public static final String PARENT_KEY = "clicheActivityParentKey";

    private String parentClassName;

    private int score;
    private TextView scoreTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_cliche);
        getActionBar().hide();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        Bundle bundle = getIntent().getExtras();

        while (bundle.containsKey(FullScreenImage.EXTRAS_KEY)) {
            bundle = bundle.getBundle(FullScreenImage.EXTRAS_KEY);
        }

        final Bundle finalBundle = bundle;

        parentClassName = bundle.getString(PARENT_KEY);

        String json = bundle.getString(CLICHE_KEY);

        Log.i(TAG, "Successfully received JSON: " + json);

        try {

            if (!InternetUtils.checkOnline(this)) {
                return;
            }
            ImageView picture = (ImageView) findViewById(R.id.picture);
            ImageView bonusPictureIv = (ImageView) findViewById(R.id.bonus_picture);

            JSONObject entity = new JSONObject(json);
            String servingUrl = entity.getString("servingUrl");
            String extraPictureServingUrl = null;
            if (entity.has("extraPictureServingUrl")) {
                extraPictureServingUrl = entity.getString("extraPictureServingUrl");
            }

            if (Strings.isNullOrEmpty(extraPictureServingUrl)) {
                // Only display servingUrl pic
                String squarePicServingUrl = servingUrl + "=s250-c";
                imageLoader.displayImage(squarePicServingUrl, picture);
                picture.setOnClickListener(
                        new ImageFullscreenClickListener(this, servingUrl, finalBundle));
                bonusPictureIv.setVisibility(View.GONE);
                // Want to set picture dimensions to 250 dp
                int dimDps = 250;
                float scale = getResources().getDisplayMetrics().density;
                // Convert dp to pixels for layout params
                int dimPx = (int) (dimDps * scale + 0.5f);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dimPx, dimPx);
                picture.setLayoutParams(params);
            } else {
                // Display serving and extra pic
                imageLoader.displayImage(servingUrl, picture);
                imageLoader.displayImage(extraPictureServingUrl, bonusPictureIv);
                picture.setOnClickListener(
                        new ImageFullscreenClickListener(this, servingUrl, finalBundle));
                bonusPictureIv.setOnClickListener(
                        new ImageFullscreenClickListener(this, extraPictureServingUrl, finalBundle));
            }

            score = finalBundle.getInt("score");
            scoreTv = (TextView) findViewById(R.id.score);
            scoreTv.setText(String.valueOf(score));

            if (entity.has("comment")) {
                String comment = entity.getString("comment");
                TextView commentTv = (TextView) findViewById(R.id.comment);
                commentTv.setText(comment);
            }

            if (entity.has("cliche")) {
                String clicheStr = entity.getString("cliche");
                TextView clicheStrTv = (TextView) findViewById(R.id.cliche_string);
                clicheStrTv.setText(clicheStr);
            } else {
                Log.e(TAG, "Cliche must have a name");
            }

            final long clicheId = entity.getLong("id");

            final List<String> tempVotedOn = finalBundle.getStringArrayList("votedOn");
            List<String> result;

            if (tempVotedOn == null) {
                result = new ArrayList<>();
            } else {
                result = new ArrayList<>(tempVotedOn);
            }

            final List<String> votedOn = result;

            ImageView upVote = (ImageView) findViewById(R.id.up_vote);
            ImageView downVote = (ImageView) findViewById(R.id.down_vote);

            upVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!InternetUtils.checkOnline(ClicheActivity.this)) {
                        return;
                    }

                    String myUserId = ClicheUtils.getUserId(ClicheActivity.this);

                    if (votedOn.contains(myUserId) || VoteCliche.votedOn.contains(clicheId)) {
                        Toast.makeText(ClicheActivity.this, "You already voted on this cliche", Toast.LENGTH_SHORT).show();
                    } else {
                        VoteCliche.votedOn.add(clicheId);
                        new VoteCliche(clicheId, myUserId, VoteCliche.VoteType.UP).execute();
                        scoreTv.setText(String.valueOf(score + 1));
                        finalBundle.putInt("score", score + 1);
                    }
                }
            });

            downVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!InternetUtils.checkOnline(ClicheActivity.this)) {
                        return;
                    }

                    String myUserId = ClicheUtils.getUserId(ClicheActivity.this);

                    if (votedOn.contains(myUserId) || VoteCliche.votedOn.contains(clicheId)) {
                        Toast.makeText(ClicheActivity.this, "You already voted on this cliche", Toast.LENGTH_SHORT).show();
                    } else {
                        VoteCliche.votedOn.add(clicheId);
                        new VoteCliche(clicheId, myUserId, VoteCliche.VoteType.DOWN).execute();
                        scoreTv.setText(String.valueOf(score - 1));
                        finalBundle.putInt("score", score - 1);
                    }
                }
            });

        } catch (JSONException e) {
            throw new RuntimeException("Problem deserializing json: " + e.getMessage());
        }
    }

    @Override
    public Intent getParentActivityIntent() {

       if (parentClassName == null) {
            throw new RuntimeException("Parent class name == null");
        }

        Intent newIntent = null;
        try {
            newIntent = new Intent(this, Class.forName(parentClassName));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

}
