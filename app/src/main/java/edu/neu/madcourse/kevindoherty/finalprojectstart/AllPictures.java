package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;

public class AllPictures extends Activity {

    ListView lvPictures;
    ProgressBar progressBar;
    TextView title;
    SwipeRefreshLayout swipeView;
    private ClicheAdapter adapter;
    private boolean tabsAdded = false;
    private boolean sortedByVote = false;

    private static final String TAG = "Cliche Feed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_pictures);

        lvPictures = (ListView) findViewById(R.id.pictures);
        progressBar = (ProgressBar) findViewById(R.id.progress_spinner);
        title = (TextView) findViewById(R.id.all_pictures_title);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeView.setEnabled(false);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.i(TAG, "Refreshing cliche feed");
                displayPictures(false);
            }
        });
        lvPictures.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0)
                    swipeView.setEnabled(true);
                else
                    swipeView.setEnabled(false);
            }
        });
        displayPictures(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayPictures(false);
    }

    protected void displayPictures(final boolean first) {

        if (!InternetUtils.checkOnline(this)) {
            progressBar.setVisibility(View.GONE);
            return;
        }

        new AsyncTask<Void, Void, List<ClicheEntity>>() {
            @Override
            protected List<ClicheEntity> doInBackground(Void... voids) {
                try {
                    Log.i(TAG, "Starting to get cliches");
                    return Endpoints.CLICHE_API.getCliches().execute().getItems();
                } catch (IOException e) {
                    Log.e(TAG, "Error getting cliches: " + e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<ClicheEntity> listEntries) {
                progressBar.setVisibility(View.GONE);
                if (listEntries == null) {
                    Toast.makeText(AllPictures.this, "Please try again", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (listEntries.isEmpty()) {
                    Toast.makeText(AllPictures.this, "No cliches have been submitted", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "Successfuly got " + listEntries.size() + " cliches");

                adapter = new ClicheAdapter(AllPictures.this, 0, listEntries);

                if (sortedByVote) {
                    adapter.sortByDescendingVotes();
                }

                lvPictures.setAdapter(adapter);

                if (!tabsAdded) {

                    final ActionBar actionBar = getActionBar();
                    // Specify that tabs should be displayed in the action bar.
                    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

                    // Create a tab listener that is called when the user changes tabs.
                    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
                        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                            if (tab.getText().toString().equals("Popular")) {
                                adapter.sortByDescendingVotes();
                                sortedByVote = true;
                            } else {
                                adapter.sortByMostRecent();
                            }
                        }

                        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                            Log.v(TAG, "Tab " + tab.getText() + " unselected");
                            // hide the given tab
                        }

                        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                            // probably ignore this event
                            Log.v(TAG, "Tab " + tab.getText() + " reselected");
                        }
                    };

                    actionBar.addTab(
                            actionBar.newTab()
                                    .setText("Recent")
                                    .setTabListener(tabListener));

                    actionBar.addTab(
                            actionBar.newTab()
                                    .setText("Popular")
                                    .setTabListener(tabListener));
                    tabsAdded = true;
                }

//                lvPictures.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        ClicheEntity clicheEntity = (ClicheEntity) adapterView.getItemAtPosition(i);
//                        Intent clicheActivity = new Intent(getApplicationContext(), ClicheActivity.class);
//                        clicheActivity.putExtra(ClicheActivity.CLICHE_KEY, new Gson().toJson(clicheEntity));
//                        clicheActivity.putExtra(ClicheActivity.PARENT_KEY, getClass().getName());
//                        Log.i(TAG, "clicheEntity.getVotedOn(): " + clicheEntity.getVotedOn());
//                        List<String> votedOn = clicheEntity.getVotedOn();
//                        ArrayList<String> votedOnArrayList;
//                        if (votedOn == null) {
//                            votedOnArrayList = new ArrayList<>();
//                        } else {
//                            votedOnArrayList = new ArrayList<>(votedOn);
//                        }
//                        clicheActivity.putStringArrayListExtra("votedOn", votedOnArrayList);
//                        int score = Integer.valueOf(((TextView) view.findViewById(R.id.score)).getText().toString());
//                        clicheActivity.putExtra("score", score);
//                        startActivity(clicheActivity);
//                    }
//                });

                title.setVisibility(View.VISIBLE);
                lvPictures.setVisibility(View.VISIBLE);
                swipeView.setRefreshing(false);
                Log.i(TAG, "Done loading cliches");

                if (first) {

                    Handler refresh = new Handler();
                    refresh.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            displayPictures(false);
                        }
                    }, 4 * 1000);
                }

            }
        }.execute();
    }
}
