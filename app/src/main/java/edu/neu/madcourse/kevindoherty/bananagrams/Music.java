package edu.neu.madcourse.kevindoherty.bananagrams;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by kdoherty on 10/8/14.
 */
public class Music {

    private static MediaPlayer mp = null;

    private boolean paused;

    /**
     * Stop old song and start new one
     */

    public static void loopMusic(Context context, int resource) {
        if (mp == null) {
            mp = MediaPlayer.create(context, resource);
        }
        mp.setLooping(true);
        mp.start();
    }

    public static void playSound(Context context, int resource) {
        MediaPlayer player = MediaPlayer.create(context, resource);
        player.start();
    }

    public static void pause(Context context) {
        if (mp != null) {
            mp.pause();
        }
    }

    /**
     * Stop the music
     */
    public static void stop(Context context) {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }
}
