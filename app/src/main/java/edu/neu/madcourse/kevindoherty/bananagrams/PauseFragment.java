package edu.neu.madcourse.kevindoherty.bananagrams;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.kevindoherty.R;

public class PauseFragment extends Fragment {

    private View.OnClickListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pause_screen, container, false);

        view.findViewById(R.id.resume).setOnClickListener(mListener);
        view.findViewById(R.id.new_game).setOnClickListener(mListener);
        view.findViewById(R.id.main_menu).setOnClickListener(mListener);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (View.OnClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
