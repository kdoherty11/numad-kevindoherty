package edu.neu.madcourse.kevindoherty.bananagrams;

import android.content.ClipData;
import android.content.Context;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by kdoherty on 10/5/14.
 */
public class OnTileTouch implements View.OnTouchListener {

    public static final int VIBRATE_LENGTH = 1; // In millis

    private Context context;

    private Vibrator vibrator;

    public OnTileTouch(Context context) {
        this.context = context;
        this.vibrator = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            if (context instanceof SinglePlayer && ((SinglePlayer)context).isGameOver()) {
                return false;
            }
            if (vibrator != null) {
                vibrator.vibrate(VIBRATE_LENGTH);
            }
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            view.setVisibility(View.INVISIBLE);
            return true;
        } else {
            return false;
        }
    }
};

