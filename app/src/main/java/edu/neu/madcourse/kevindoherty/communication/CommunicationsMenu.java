package edu.neu.madcourse.kevindoherty.communication;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.api.client.util.Strings;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmActivity;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmUtils;

public class CommunicationsMenu extends GcmActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_communications_menu);
        findViewById(R.id.comm_active_users).setOnClickListener(this);
        findViewById(R.id.comm_leaderboard_button).setOnClickListener(this);
        findViewById(R.id.comm_ack_button).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.communications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.comm_active_users:
                if (PreferenceManager.getDefaultSharedPreferences(this).contains("name") &&
                        !Strings.isNullOrEmpty(GcmUtils.getRegistrationId(this))) {
                    Intent activePlayers = new Intent(this, RegisteredPlayers.class);
                    startActivity(activePlayers);
                } else {
                    Intent reg = new Intent(this, Register.class);
                    startActivity(reg);
                }
                break;
            case R.id.comm_leaderboard_button:
                Intent leaderboard = new Intent(this, Leaderboard.class);
                startActivity(leaderboard);
                break;
            case R.id.comm_ack_button:
                Intent ack = new Intent(this, CommAck.class);
                startActivity(ack);
                break;

        }
    }
}
