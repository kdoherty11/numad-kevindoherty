package edu.neu.madcourse.kevindoherty.dictionary;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.kevindoherty.MainActivity;
import edu.neu.madcourse.kevindoherty.R;

public class TestDictionary extends Activity implements View.OnClickListener, TextWatcher {

    private static String FOUND_WORDS_KEY = "foundWordsKey";
    private static String TEST_WORD_KEY = "testWordKey";

    private EditText mTestWordEt;
    private GridView mFoundWordsGv;
    private Dictionary mDict;
    private ArrayAdapter<String> mAdapter;

    private List<String> mFoundWords = new ArrayList<>();


    // private static final String TAG = "DictionaryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        mDict = new Dictionary(this);

        mTestWordEt = (EditText) findViewById(R.id.test_word);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(FOUND_WORDS_KEY)) {
                mFoundWords = savedInstanceState.getStringArrayList(FOUND_WORDS_KEY);
            }
            if (savedInstanceState.containsKey(TEST_WORD_KEY)) {
                mTestWordEt.setText(savedInstanceState.getString(TEST_WORD_KEY));
            }
        }

        mTestWordEt.addTextChangedListener(this);

        mFoundWordsGv = (GridView) findViewById(R.id.word_grid);
        mAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, mFoundWords);
        mFoundWordsGv.setAdapter(mAdapter);

        findViewById(R.id.clear_button).setOnClickListener(this);
        findViewById(R.id.menu_button).setOnClickListener(this);
        findViewById(R.id.ack_button).setOnClickListener(this);
    }

    private void addToWordGrid(String word) {
        String trimmed = word.trim();
        if (!mFoundWords.contains(trimmed)) {
            mFoundWords.add(trimmed);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, mFoundWords);
            mFoundWordsGv.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dictionary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.clear_button:
                mTestWordEt.setText("");
                mFoundWords.clear();
                mFoundWordsGv.setAdapter(mAdapter);
                break;
            case R.id.menu_button:
                Intent menu = new Intent(this, MainActivity.class);
                startActivity(menu);
                break;
            case R.id.ack_button:
                Intent ack = new Intent(this, DictAck.class);
                startActivity(ack);
                break;
            default:
                throw new IllegalStateException("Default case in on DictionaryActivity onClick");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        // Do nothing
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int len = charSequence.length();
        if (len >= 3) {
            if (mDict.isWord(charSequence.toString())) {
                beep();
                mTestWordEt.setTextColor(getResources().getColor(R.color.green));
                addToWordGrid(charSequence.toString());
            } else {
                mTestWordEt.setTextColor(getResources().getColor(R.color.red));
            }
        } else {
            mTestWordEt.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // Do nothing
    }

    private void beep() {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.beep);
        mp.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putStringArrayList(FOUND_WORDS_KEY, (ArrayList) mFoundWords);
        bundle.putString(TEST_WORD_KEY, mTestWordEt.getText().toString());
    }
}
