package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by kdoherty on 12/6/14.
 */
public final class ImageUtils {

    private static final String TAG = "Image Utils";

    private ImageUtils() { }

    private static Bitmap rotateImage(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static void setPic(String imagePath, ImageView imageView) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        if (photoW == 0 || photoH == 0) {
            throw new IllegalStateException("Photo from path " + imagePath + " has a dimension of 0");
        }

        int scaleFactor = -1;

        if (targetW == 0 || targetH == 0) {
            Log.e(TAG, "Target image has a width or height of 0");
            scaleFactor = 1;
        } else {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);

        ExifInterface ei;
        try {
            ei = new ExifInterface(imagePath);
        } catch (IOException e) {
            Log.e(TAG, "Error creating the ExifInterface: " + e.getMessage());
            return;
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        Bitmap rotatedBm;
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                Log.d(TAG, "Rotating image 90 degrees");
                rotatedBm = rotateImage(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                Log.d(TAG, "Rotating image 180 degrees");
                rotatedBm = rotateImage(bitmap, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                Log.d(TAG, "Rotating image 270 degrees");
                rotatedBm = rotateImage(bitmap, 270);
                break;
            default:
                Log.w(TAG, "Default rotation used. Orientation: " + orientation);
                rotatedBm = bitmap;
        }

        imageView.setImageBitmap(rotatedBm);
    }


}
