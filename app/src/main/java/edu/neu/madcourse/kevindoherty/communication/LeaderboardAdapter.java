package edu.neu.madcourse.kevindoherty.communication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kdoherty.gcm.endpoint.leaderboard.model.LeaderboardEntry;

import java.util.List;

import edu.neu.madcourse.kevindoherty.R;

/**
 * Created by kdoherty on 10/30/14.
 */
public class LeaderboardAdapter extends ArrayAdapter<LeaderboardEntry> {

    List<LeaderboardEntry> entries;
    Context context;


    public LeaderboardAdapter(Context context, int resource, List<LeaderboardEntry> entries) {
        super(context, resource, entries);
        this.entries = entries;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.leaderboard_entry, null);
        }


        LeaderboardEntry entry = entries.get(position);

        if (entry != null) {

            TextView score = (TextView) v.findViewById(R.id.score);
            TextView name = (TextView) v.findViewById(R.id.name);
            TextView date = (TextView) v.findViewById(R.id.date);

            score.setText(String.valueOf(entry.getScore()));
            name.setText(entry.getName());
            date.setText(entry.getDate());
        }

        return v;
    }




}
