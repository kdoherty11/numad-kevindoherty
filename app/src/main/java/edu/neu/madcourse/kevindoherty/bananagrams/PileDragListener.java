package edu.neu.madcourse.kevindoherty.bananagrams;

import android.view.DragEvent;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/6/14.
 */
public class PileDragListener implements View.OnDragListener {

    private SinglePlayer context;

    public PileDragListener(SinglePlayer context) {
        this.context = context;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
            case DragEvent.ACTION_DROP:
                View view = (View) event.getLocalState();
                if (view instanceof TileView) {
                    TileView tileView = (TileView) view;
                    Tile tile = tileView.getTile();
                    if (tile == Tile.MT) {
                        Toast.makeText(context, "Tile is empty", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    int holderIndex = tileView.getHolderIndex();
                    if (holderIndex == -1) {
                        // From board
                        context.getBoard().removeTile(tileView.getRow(), tileView.getCol());
                        context.refreshBoard();
                    } else {
                        // From holder
                        context.getTileHolderAdapter().remove(holderIndex);
                    }

                    List<Tile> newTiles = context.getGame().dumpTile(tile);
                    if (newTiles.isEmpty()) {
                        Toast.makeText(context, "Not enough tiles left!", Toast.LENGTH_SHORT).show();
                    }
                    for (Tile newTile : newTiles) {
                        context.getTileHolderAdapter().addTile(newTile);
                    }
                    context.refreshTileHolder();
                    context.refreshPileSize();
                    break;
                }
            default:
                return false;
        }
        return true;
    }
}
