package edu.neu.madcourse.kevindoherty.bananagrams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.kevindoherty.MainActivity;
import edu.neu.madcourse.kevindoherty.R;

public class BananagramsMenu extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bananagrams_menu);
        findViewById(R.id.bgrams_new_game_button).setOnClickListener(this);
        findViewById(R.id.bgrams_ack_button).setOnClickListener(this);
        findViewById(R.id.bgrams_rules_button).setOnClickListener(this);
        findViewById(R.id.bgrams_quit_button).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bananagrams_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.bgrams_new_game_button:
                Intent singlePlayer = new Intent(this, SinglePlayer.class);
                startActivity(singlePlayer);
                break;
            case R.id.bgrams_ack_button:
                Intent ack = new Intent(this, BgramsAck.class);
                startActivity(ack);
                break;
            case R.id.bgrams_rules_button:
                Intent rules = new Intent(this, BgramsRules.class);
                startActivity(rules);
                break;
            case R.id.bgrams_quit_button:
                Intent mainMenu = new Intent(this, MainActivity.class);
                startActivity(mainMenu);
                break;
            default:
                assert(false);
        }
    }
}
