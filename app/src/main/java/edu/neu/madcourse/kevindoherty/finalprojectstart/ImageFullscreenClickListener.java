package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by kdoherty on 11/29/14.
 */
public class ImageFullscreenClickListener implements View.OnClickListener {

    private Activity context;
    private String servingUrl;
    private Bundle extras;

    private static final String TAG = "Cliche Image Fullscreen Click Listener";

    public ImageFullscreenClickListener(Activity context, String servingUrl) {
        this(context, servingUrl, null);
    }

    public ImageFullscreenClickListener(Activity context, String servingUrl, Bundle extras) {
        this.context = context;
        this.servingUrl = servingUrl;
        this.extras = extras;
    }

    @Override
    public void onClick(View view) {
        Intent fullScreenIntent = new Intent(context, FullScreenImage.class);
        fullScreenIntent.putExtra(FullScreenImage.IMAGE_KEY, servingUrl);
        fullScreenIntent.putExtra(FullScreenImage.PARENT_KEY, context.getClass().getName());
        fullScreenIntent.putExtra(FullScreenImage.EXTRAS_KEY, extras);
        context.startActivity(fullScreenIntent);
    }
}
