package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;
import com.kdoherty.cliche.endpoint.cliche.model.JsonMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;

public class AddDescription extends Activity implements View.OnClickListener {

    public static final String PREFS_NAME = "CommercialClichePrefs";
    private static final String ON_CLICHE = "Attempting this cliche num:";
    public static final String FIRST_IMAGE_PATH = "Path_to_image";
    public static final String BONUS_IMAGE_PATH = "BonusImagePath";
    private static final String ACTIVITY_RESULT_DESCRIPTION = "Users_description";
    private static final String TAG = "Add Cliche Description: ";
    public static final String LEAVE_PAGE_WARNING = "Are you sure you want to leave this page? Unsaved data will be lost.";

    // Max num of chars in cliche description
    public static final int maxDescriptionLen = 60;

    private String cliche;
    private TextView title;
    protected EditText description;
    private ImageView photo;
    private String mFirstPhotoPath = null;
    private String mBonusImagePath = null;
    private boolean mHasBonus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_add_description);
        getActionBar().hide();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        cliche = settings.getString(ON_CLICHE, "No cliche selected");

        title = (TextView) findViewById(R.id.add_descr_cliche_string);
        description = (EditText) findViewById(R.id.add_descr_comment);

        findViewById(R.id.add_descr_publish_button).setOnClickListener(this);
        findViewById(R.id.add_descr_cancel_button).setOnClickListener(this);

        title.setText(cliche);

        photo = (ImageView) findViewById(R.id.add_descr_picture);
        mFirstPhotoPath = getIntent().getStringExtra(FIRST_IMAGE_PATH);

        mBonusImagePath = getIntent().getStringExtra(BONUS_IMAGE_PATH);
        mHasBonus = !Strings.isNullOrEmpty(mBonusImagePath);
        float scale = getResources().getDisplayMetrics().density;
        int dimDps = 250;
        int dimPx = (int) (dimDps * scale + 0.5f);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dimPx, dimPx);
        photo.setLayoutParams(params);

        photo.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Remove it here unless you want to get this callback for EVERY
                //layout pass, which can get you into infinite loops if you ever
                //modify the layout from within this method.
                photo.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                ImageUtils.setPic(mFirstPhotoPath, photo);
                Log.i(TAG, "getPic called");
                //Now you can get the width and height from content
            }
        });
//
        description.setOnKeyListener(new View.OnKeyListener() {
            /**
             * This listens for the user to press the enter button on
             * the keyboard and then hides the virtual keyboard
             */
            public boolean onKey(View arg0, int arg1, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (arg1 == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(description.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });


        Log.i(TAG, "onCreate over");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.add_descr_publish_button:
                publish(String.valueOf(description.getText()));
                break;
            case R.id.add_descr_cancel_button:
                areYouSure();
                break;
        }
    }

    public void areYouSure() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Intent playScreen = new Intent(getApplicationContext(), SelectCliche.class);
                        startActivity(playScreen);
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(LEAVE_PAGE_WARNING).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed Called");
        areYouSure();
    }

    public void publish(String descr) {

        if (Strings.isNullOrEmpty(descr)) {
            Toast.makeText(this, "Please add a description", Toast.LENGTH_SHORT).show();
            return;
        } else if (descr.length() > maxDescriptionLen) {
            Toast.makeText(this, "Cliche is description is too long", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!InternetUtils.checkOnline(this)) {
            return;
        }

        uploadClicheToServer(cliche, descr);

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (!prefs.contains("submittedCliche")) {
            prefs.edit().putBoolean("submittedCliche", true).apply();
        }

        Intent playScreen = new Intent(getApplicationContext(), SelectCliche.class);
        playScreen.putExtra("displayToast", true);
        startActivity(playScreen);
    }

    private void uploadClicheToServer(final String title, final String description) {

        if (!InternetUtils.checkOnline(this)) {
            return;
        }

        new AsyncTask<Void, Void, String[]>() {

            @Override
            protected String[] doInBackground(Void... voids) {
                try {

                    final String[] result = new String[2];
                    JsonMap res = Endpoints.CLICHE_API.getUploadUrl().execute();
                    String uploadUrl = (String) res.get("uploadUrl");
                    result[0] = uploadUrl;
                    Log.i(TAG, "Upload URL returned from server: " + uploadUrl);

                    if (mHasBonus) {
                        JsonMap bonusRes = Endpoints.CLICHE_API.getUploadUrl().execute();
                        String bonusUploadUrl = (String) bonusRes.get("uploadUrl");
                        result[1] = bonusUploadUrl;
                        Log.i(TAG, "Bonus Upload URL returned from server: " + bonusUploadUrl);
                    }


                    if (mHasBonus) {
                        JsonMap bonusRes = Endpoints.CLICHE_API.getUploadUrl().execute();
                        String bonusUploadUrl = (String) bonusRes.get("uploadUrl");
                        result[1] = bonusUploadUrl;
                        Log.i(TAG, "Bonus Upload URL returned from server: " + bonusUploadUrl);
                    }

                    return result;
                } catch (IOException e) {
                    throw new RuntimeException("Error getting upload url " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String[] uploadUrls) {
                sendImagesToUploadUrls(uploadUrls, title, description);
            }


        }.execute();
    }

    private HttpResponse uploadImageToUrl(String imagePath, String url) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        File f = new File(imagePath);
        FileBody fileBody = new FileBody(f);
        builder.addPart("file", fileBody);

        HttpEntity entity = builder.build();
        httppost.setEntity(entity);

        HttpResponse response = null;
        try {
            response = httpclient.execute(httppost);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Problem uploading image");
        }
        return response;
    }

    protected void sendImagesToUploadUrls(final String[] uploadUrls, final String title, final String comment) {
        new AsyncTask<Void, Void, JSONObject>() {

            @Override
            protected JSONObject doInBackground(Void... voids) {
                try {
                    Log.d(TAG, "Sending image bytes to upload URL");
                    HttpResponse firstResponse = uploadImageToUrl(mFirstPhotoPath, uploadUrls[0]);
                    String firstJsonStr = EntityUtils.toString(firstResponse.getEntity());
                    JSONObject json = new JSONObject(firstJsonStr);
                    if (!mHasBonus) {
                        return json;
                    } else {
                        HttpResponse bonusResponse = uploadImageToUrl(mBonusImagePath, uploadUrls[1]);
                        String bonusJsonStr = EntityUtils.toString(bonusResponse.getEntity());
                        JSONObject bonusJson = new JSONObject(bonusJsonStr);

                        Iterator<String> keyItr = bonusJson.keys();
                        while (keyItr.hasNext()) {
                            String key = keyItr.next();
                            json.put("bonus" + key, bonusJson.get(key));
                        }
                        return json;
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Problem uploading image " + e.getMessage());
                } catch (JSONException jsonException) {
                    throw new RuntimeException("Problem parsing json string: " + jsonException.getMessage());
                }
            }

            @Override
            protected void onPostExecute(JSONObject resultJson) {
                storeCliche(resultJson, title, comment);
            }
        }.execute();
    }

    private void storeCliche(final JSONObject resultJson, final String title, final String comment) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    final String blobKey = resultJson.getString("blobKey");
                    final String servingUrl = resultJson.getString("servingUrl");

                    ClicheEntity clicheEntity = new ClicheEntity();
                    clicheEntity.setUserId(ClicheUtils.getUserId(AddDescription.this));
                    clicheEntity.setServingUrl(servingUrl);
                    clicheEntity.setBlobKey(blobKey);
                    clicheEntity.setCliche(title);
                    clicheEntity.setComment(comment);

                    if (resultJson.has("bonusblobKey")) {
                        clicheEntity.setExtraPictureBlobKey(resultJson.getString("bonusblobKey"));
                    }

                    if (resultJson.has("bonusservingUrl")) {
                        clicheEntity.setExtraPictureServingUrl(resultJson.getString("bonusservingUrl"));
                        Log.i(TAG, "***** Storing cliche entity with bonus picture's serving URL *****");
                    }

                    Endpoints.CLICHE_API.storeCliche(clicheEntity).execute();
                    Log.i(TAG, "Successfully stored image");

                } catch (Exception e) {
                    throw new RuntimeException("Error storing image: " + e.getMessage());
                }

                return null;
            }
        }.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_description, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}