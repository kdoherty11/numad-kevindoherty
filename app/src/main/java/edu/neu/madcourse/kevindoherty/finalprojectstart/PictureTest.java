package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.kdoherty.cliche.endpoint.cliche.model.ClicheEntity;
import com.kdoherty.cliche.endpoint.cliche.model.JsonMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.neu.madcourse.kevindoherty.R;

public class PictureTest extends Activity implements View.OnClickListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;

    private static final String TAG = "Cliche Test";

    // Storage for camera image URI components
    private final static String CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI";

    // Required for camera operations in order to save the image file on resume.
    private String mCurrentPhotoPath = null;
    private Uri mCapturedImageURI = null;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_picture_test);

        mImageView = (ImageView) findViewById(R.id.test_picture);

        findViewById(R.id.take_picture).setOnClickListener(this);
        findViewById(R.id.my_pictures).setOnClickListener(this);
        findViewById(R.id.all_pictures).setOnClickListener(this);
        findViewById(R.id.clear_pictures).setOnClickListener(this);
        findViewById(R.id.cliche_acknowledgements).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.picture_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                Log.i(TAG, "Successfully created picture file");
            } catch (IOException ex) {
                Log.e(TAG, "Error occured while creating picture file: " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "On Activity Result");
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            addPhotoToGallery();
            setPic();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.take_picture:
                dispatchTakePictureIntent();
                break;
            case R.id.my_pictures:
                Intent myPics = new Intent(getApplicationContext(), MyPictures.class);
                startActivity(myPics);
                break;
            case R.id.all_pictures:
                Intent allPics = new Intent(getApplicationContext(), AllPictures.class);
                startActivity(allPics);
                break;
            case R.id.clear_pictures:
                clearAllPictures();
                break;
            case R.id.cliche_acknowledgements:
                Intent ack = new Intent(getApplicationContext(), ClicheAcknowledgements.class);
                startActivity(ack);
        }

    }

    private void clearAllPictures() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Endpoints.CLICHE_API.clearCliches().execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void arg) {
                Toast.makeText(PictureTest.this, "Pictures Cleared", Toast.LENGTH_LONG).show();
            }

        }.execute();
    }

    private void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        if (photoW == 0 || photoH == 0) {
            throw new IllegalStateException("Photo has a dimension of 0");
        }

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        ExifInterface exif;
        try {
            exif = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            Log.e(TAG, "Error creating the ExifInterface: " + e.getMessage());
            return;
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        Log.i(TAG, "Orientation: " + orientation);

        Matrix matrix = new Matrix();
        if (orientation != ExifInterface.ORIENTATION_NORMAL) {
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }

        mImageView.setImageBitmap(bitmap);

        uploadClicheToServer("Fast food made to look healthy", "Lol it was a McDonalds commercial");
    }


    private void uploadClicheToServer(final String title, final String comment) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    JsonMap res = Endpoints.CLICHE_API.getUploadUrl().execute();
                    String uploadUrl = (String) res.get("uploadUrl");
                    Log.i(TAG, "Upload URL returned from server: " + uploadUrl);
                    return uploadUrl;
                } catch (IOException e) {
                    throw new RuntimeException("Error getting upload url " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String uploadUrl) {
                sendImageToUploadUrl(uploadUrl, title, comment);
            }


        }.execute();
    }

    private void sendImageToUploadUrl(final String uploadUrl, final String title, final String comment) {
        new AsyncTask<Void, Void, JSONObject>() {

            @Override
            protected JSONObject doInBackground(Void... voids) {
                try {
                    Log.d(TAG, "Sending image bytes to upload URL");
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(uploadUrl);

                    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

                    File f = new File(mCurrentPhotoPath);
                    FileBody fileBody = new FileBody(f);
                    builder.addPart("file", fileBody);

                    HttpEntity entity = builder.build();
                    httppost.setEntity(entity);

                    HttpResponse response = httpclient.execute(httppost);
                    String jsonStr = EntityUtils.toString(response.getEntity());
                    JSONObject json = new JSONObject(jsonStr);
                    return json;

                } catch (IOException e) {
                    throw new RuntimeException("Problem uploading image " + e.getMessage());
                } catch (JSONException jsonException) {
                    throw new RuntimeException("Problem parsing json string: " + jsonException.getMessage());
                }
            }

            @Override
            protected void onPostExecute(JSONObject resultJson) {
                storeCliche(resultJson, title, comment);
            }
        }.execute();
    }

    private void storeCliche(final JSONObject resultJson, final String title, final String comment) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    final String blobKey = resultJson.getString("blobKey");
                    final String servingUrl = resultJson.getString("servingUrl");

                    JsonMap requestJson = new JsonMap();
                    requestJson.put("blobKey", blobKey);
                    requestJson.put("servingUrl", servingUrl);
                    requestJson.put("userId", ClicheUtils.getUserId(PictureTest.this));

                    ClicheEntity clicheEntity = new ClicheEntity();
                    clicheEntity.setUserId(ClicheUtils.getUserId(PictureTest.this));
                    clicheEntity.setServingUrl(servingUrl);
                    clicheEntity.setBlobKey(blobKey);
                    clicheEntity.setCliche(title);
                    clicheEntity.setComment(comment);

                    Endpoints.CLICHE_API.storeCliche(clicheEntity).execute();
                    Log.i(TAG, "Successfully stored image");

                } catch (Exception e) {
                    throw new RuntimeException("Error storing image: " + e.getMessage());
                }

                return null;
            }
        }.execute();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mCurrentPhotoPath != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_PATH_KEY, mCurrentPhotoPath);
        }
        if (mCapturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, mCapturedImageURI.toString());
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            mCurrentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

}
