package edu.neu.madcourse.kevindoherty.finalprojectstart;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.kdoherty.cliche.endpoint.cliche.Cliche;

import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmActivity;

/**
 * Created by kdoherty on 11/14/14.
 */
public class Endpoints {

    public static Cliche CLICHE_API;

    static {
        Cliche.Builder builder = new Cliche.Builder(
                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                .setApplicationName(GcmActivity.APP_NAME);

        CLICHE_API = builder.build();

    }
}
