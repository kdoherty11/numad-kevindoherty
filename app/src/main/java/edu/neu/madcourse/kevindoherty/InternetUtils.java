package edu.neu.madcourse.kevindoherty;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by kdoherty on 10/26/14.
 */
public class InternetUtils {

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static boolean checkOnline(Context context) {
        return checkOnline(context, "Please connect to the internet and try again");
    }

    public static boolean checkOnline(Context context, String errMsg) {
        boolean isOnline = isOnline(context);
        if (!isOnline) {
            Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show();
        }
        return isOnline;
    }

}
