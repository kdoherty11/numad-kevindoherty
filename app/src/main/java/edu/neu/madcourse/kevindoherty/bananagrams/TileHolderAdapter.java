package edu.neu.madcourse.kevindoherty.bananagrams;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/6/14.
 */
public class TileHolderAdapter extends BaseAdapter {

    private Context context;

    /** Contains all Pieces which have been taken off the Board */
    private List<Tile> tiles = new ArrayList<>();

    TileHolderAdapter(Context context) {
        this(context, new ArrayList<Tile>());
    }

    TileHolderAdapter(Context context, List<Tile> startingTiles) {
        this.context = context;
        this.tiles = startingTiles;
    }

    @Override
    public int getCount() {
        return tiles.size();
    }

    @Override
    public Object getItem(int position) {
        return tiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    void addTile(Tile tile) {
        if (tile == Tile.MT) {
            throw new IllegalArgumentException("Can't display an empty tile");
        }
        for (int i = 0; i < tiles.size(); i++) {
            if (tiles.get(i) == Tile.MT) {
                tiles.set(i, tile);
                notifyDataSetChanged();
                return;
            }
        }
        tiles.add(tile);
        notifyDataSetChanged();
    }

    public int getNonNullCount() {
        int numTiles = tiles.size();
        int count = 0;
        for (int i = 0; i < numTiles; i++) {
            if (tiles.get(i) != Tile.MT) {
                count++;
            }
        }
        return count;
    }

    public boolean isEmpty() {
        return getNonNullCount() == 0;
    }

    public Tile remove(int index) {
        return tiles.set(index, Tile.MT);
    }

    public void addAtIndex(int index, Tile tile) {
        Tile temp = tiles.get(index);
        if (temp == Tile.MT) {
            tiles.set(index, tile);
        } else {
            tiles.add(index, tile);
        }
    }

    public void addTileAtEnd(Tile tile) {
        tiles.add(tile);
    }

    public void swap(int index1, int index2) {
        Tile temp = tiles.get(index1);
        tiles.set(index1, tiles.get(index2));
        tiles.set(index2, temp);
    }

    static class ViewHolder {
        ImageView tileView;
        TextView tileLetter;
    }

    /**
     * Manually creates an ImageView to represent the Piece contained at the
     * index specified by the input position.
     *
     * @param position
     *            The position in the GridView
     * @param convertView
     *            Used to so we don't recreate the ImageView every time this is
     *            called
     * @param parent
     *            The parent of the view
     *
     * @return The Image representation of the Piece contained at the index
     *         specified by the input position
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = (TileView) inflater.inflate(R.layout.tile, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tileView = (ImageView) convertView.findViewById(R.id.tile_background);
            viewHolder.tileLetter = (TextView) convertView.findViewById(R.id.tile_letter);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Tile tile = tiles.get(position);
        if (tile != Tile.MT) {
            viewHolder.tileLetter.setText(String.valueOf(tile.getLetter()));
            ((TileView) convertView).setTile(tile);
            if (!((SinglePlayer)context).isGameOver()) {
                convertView.setOnTouchListener(new OnTileTouch(context));
            }
        } else {
            viewHolder.tileView.setBackgroundColor(context.getResources().getColor(R.color.tile_holder_bg));
        }
        ((TileView) convertView).setRow(-1);
        ((TileView) convertView).setCol(-1);
        ((TileView) convertView).setHolderIndex(position);
        convertView.setOnDragListener(new HolderDragListener(this, (SinglePlayer) context, position));

        return convertView;
    }
}
