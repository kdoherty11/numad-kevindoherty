package edu.neu.madcourse.kevindoherty.communication;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.kdoherty.gcm.endpoint.leaderboard.model.LeaderboardEntry;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.Endpoints;

public class Leaderboard extends Activity {

    ListView lvLeaderboard;

    private static final String TAG = "Leaderboard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        lvLeaderboard = (ListView) findViewById(R.id.leaderboard);
        displayLeaderboard();
    }

    private void displayLeaderboard() {
        new AsyncTask<Void, Void, List<LeaderboardEntry>>() {
            @Override
            protected List<LeaderboardEntry> doInBackground(Void... voids) {
                try {
                    return Endpoints.LEADERBOARD.getHighScores().execute().getItems();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<LeaderboardEntry> listEntries) {
                if (listEntries == null || listEntries.isEmpty()) {
                    Toast.makeText(Leaderboard.this, "No scores have been submitted", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (LeaderboardEntry lbEntry : listEntries) {
                    Log.i(TAG, "LB Entry Name: " + lbEntry.getName());
                    Log.i(TAG, "LB Entry Score: " + lbEntry.getScore());
                }
                lvLeaderboard.setAdapter(new LeaderboardAdapter(Leaderboard.this, 0, listEntries));

            }
        }.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.leaderboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
