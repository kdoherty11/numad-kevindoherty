package edu.neu.madcourse.kevindoherty.bananagrams.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kdoherty on 10/2/14.
 */
public enum Tile implements Parcelable {

    A('A', 1),
    B('B', 3),
    C('C', 3),
    D('D', 2),
    E('E', 1),
    F('F', 4),
    G('G', 2),
    H('H', 4),
    I('I', 1),
    J('J', 8),
    K('K', 5),
    L('L', 1),
    M('M', 3),
    N('N', 1),
    O('O', 1),
    P('P', 3),
    Q('Q', 10),
    R('R', 1),
    S('S', 1),
    T('T', 1),
    U('U', 1),
    V('V', 4),
    W('W', 4),
    X('X', 8),
    Y('Y', 4),
    Z('Z', 10),
    MT(' ', 0);

    private final char letter;
    private final int points;

    private Tile(char letter, int points) {
        this.letter = letter;
        this.points = points;
    }

    @Override
    public String toString() {
        if (this == Tile.MT) {
            return "";
        } else {
            return super.toString();
        }
    }

    public char getLetter() {
        return letter;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }

    public static final Creator<Tile> CREATOR = new Creator<Tile>() {
        @Override
        public Tile createFromParcel(final Parcel source) {
            return Tile.values()[source.readInt()];
        }

        @Override
        public Tile[] newArray(final int size) {
            return new Tile[size];
        }
    };

//    public static class TileSerializer implements JsonSerializer<Tile> {
//
//        @Override
//        public JsonElement serialize(Tile src, Type typeOfSrc, JsonSerializationContext context) {
//            if (src == Tile.MT) {
//                return
//            }
//            return null;
//        }
//    }

}
