package edu.neu.madcourse.kevindoherty.bananagrams;

import android.view.DragEvent;
import android.view.View;
import android.widget.Toast;

import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Board;
import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/6/14.
 */
public class BoardDragListener implements View.OnDragListener {

    private SinglePlayer mContext;
    private Board mBoard;
    private int row;
    private int col;

    public BoardDragListener(SinglePlayer context, Board board, int row, int col) {
        this.mContext = context;
        this.mBoard = board;
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        final TileView view = (TileView) event.getLocalState();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                if (view != null) {
                    view.post(new Runnable() {
                        @Override
                        public void run() {
                            view.setVisibility(View.VISIBLE);
                        }
                    });
                }
                break;
            case DragEvent.ACTION_DROP:
                if (view == null) {
                    return false;
                }
                int fromRow = view.getRow();
                int fromCol = view.getCol();

                if (view.getHolderIndex() != -1) {
                    // Coming from the tile holder
                    Tile prev = mBoard.placeTile(row, col, ((TileView)view).getTile());
                    Tile removed = mContext.getTileHolderAdapter().remove(view.getHolderIndex());
                    if (prev != Tile.MT) {
                        // Replacing a tile on the board
                        mContext.getTileHolderAdapter().addAtIndex(view.getHolderIndex(), prev);
                    }
                    mContext.refreshTileHolder();
                } else {
                    if (mBoard.getTile(row, col) == Tile.MT) {
                        mBoard.swapTiles(fromRow, fromCol, row, col);
                    } else {
                        return false;
                    }
                }

                if (mContext.areValidWordsFromSquare(row, col)) {
                    Toast.makeText(mContext, "Word made!", Toast.LENGTH_SHORT).show();
                    Music.playSound(mContext, R.raw.ding);
//                    BoardAdapter.validWords[row][col] = true;
                }
//
//                if (mContext.isHorizontalValid(row, col)) {
//                    // Set all tiles to left and right to true
//                }
//                if (mContext.isVerticalValid(row, col)) {
//                    // Set all tiles above and below to true
//                }

                if (mContext.getTileHolderAdapter().isEmpty() && mContext.areAllValidWords() &&
                        mContext.isAllConnected()) {
                    // Deal 1 tile
                    mContext.getTileHolderAdapter().addTile(mContext.getGame().takeTile());
                    mContext.refreshTileHolder();
                    mContext.refreshPileSize();
                }
                mContext.refreshScore();
                mContext.refreshBoard();
                break;
            default:
                return false;
        }
        return true;
    }
}
