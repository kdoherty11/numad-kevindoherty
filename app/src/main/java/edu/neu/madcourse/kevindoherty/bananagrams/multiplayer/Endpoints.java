package edu.neu.madcourse.kevindoherty.bananagrams.multiplayer;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.kdoherty.gcm.endpoint.games.Games;
import com.kdoherty.gcm.endpoint.leaderboard.Leaderboard;
import com.kdoherty.gcm.endpoint.messaging.Messaging;
import com.kdoherty.gcm.endpoint.registration.Registration;

/**
 * Created by kdoherty on 10/29/14.
 */
public class Endpoints {

    public static Messaging.MessagingEndpoint MESSAGING;
    public static Games.GamesEndpoint GAMES;
    public static Registration REG;
    public static Leaderboard LEADERBOARD;

    static {
        Messaging.Builder builder = new Messaging.Builder(
                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                .setApplicationName(GcmActivity.APP_NAME);

        MESSAGING = builder.build().messagingEndpoint();

        Games.Builder gamesBuilder = new Games.Builder(
                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                .setApplicationName(GcmActivity.APP_NAME);

        GAMES = gamesBuilder.build().gamesEndpoint();

        Registration.Builder regBuilder = new Registration.Builder(
                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                .setApplicationName(GcmActivity.APP_NAME);
        REG = regBuilder.build();

        Leaderboard.Builder lbBuilder = new Leaderboard.Builder(
                AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                .setApplicationName(GcmActivity.APP_NAME);
        LEADERBOARD = lbBuilder.build();
    }

}
