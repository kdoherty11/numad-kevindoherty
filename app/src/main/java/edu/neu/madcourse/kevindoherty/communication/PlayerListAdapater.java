package edu.neu.madcourse.kevindoherty.communication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.kdoherty.gcm.endpoint.registration.model.RegistrationRecord;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.Endpoints;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmUtils;

/**
 * Created by kdoherty on 10/29/14.
 */
public class PlayerListAdapater extends ArrayAdapter<RegistrationRecord> implements AdapterView.OnItemClickListener {

    // declaring our ArrayList of items
    private List<RegistrationRecord> records;

    private Context context;

    static String userName;


    public PlayerListAdapater(Context context, int textViewResourceId, List<RegistrationRecord> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.records = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.reg_record, null);
        }

        RegistrationRecord i = records.get(position);

        if (i != null) {

            TextView name = (TextView) v.findViewById(R.id.name);

            if (!Strings.isNullOrEmpty(i.getName())) {
                name.setText(i.getName());
            } else {
                name.setText("Unknown");
            }
        }

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        RegistrationRecord i = records.get(position);

        final String targetRegId = i.getRegId();

        if (Strings.isNullOrEmpty(userName)) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            userName = prefs.getString("name", "Unknown");
        }

        if (!Strings.isNullOrEmpty(targetRegId)) {
            new AsyncTask<Void, Void, Boolean>() {

                @Override
                protected Boolean doInBackground(Void... voids) {
                    try {
                        if (InternetUtils.checkOnline(context)) {
                            Endpoints.MESSAGING.sendMessageToIdWithRegId(userName + " has challange you to a game of bananagrams!",
                                    targetRegId,
                                    GcmUtils.getRegistrationId(context)).execute();
                            return true;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return false;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    if (result) {
                        Toast.makeText(context, "Sent game invite.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Sending game invite failed.", Toast.LENGTH_SHORT).show();
                    }
                }
            }.execute();
        }
    }
}
