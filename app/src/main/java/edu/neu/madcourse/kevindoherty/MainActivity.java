package edu.neu.madcourse.kevindoherty;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.kevindoherty.bananagrams.BananagramsMenu;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.MultiplayerMenu;
import edu.neu.madcourse.kevindoherty.communication.CommunicationsMenu;
import edu.neu.madcourse.kevindoherty.dictionary.TestDictionary;
import edu.neu.madcourse.kevindoherty.finalprojectstart.CommercialClicheHome;
import edu.neu.madcourse.kevindoherty.finalprojectstart.PictureTest;
import edu.neu.madcourse.kevindoherty.finalprojectstart.PlayStoreDescription;
import edu.neu.madcourse.kevindoherty.finalprojectstart.SelectCliche;
import edu.neu.madcourse.kevindoherty.sudoku.Sudoku;


public class MainActivity extends Activity implements View.OnClickListener {

    // private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.action_bar_title));
        }
        findViewById(R.id.about_button).setOnClickListener(this);
        findViewById(R.id.error_button).setOnClickListener(this);
        findViewById(R.id.sudoku_button).setOnClickListener(this);
        findViewById(R.id.dictionary_button).setOnClickListener(this);
        findViewById(R.id.word_game_button).setOnClickListener(this);
        findViewById(R.id.communication_button).setOnClickListener(this);
        findViewById(R.id.two_player_word_game_button).setOnClickListener(this);
        findViewById(R.id.trickiest_part_button).setOnClickListener(this);
        findViewById(R.id.commercial_cliche_button).setOnClickListener(this);
        findViewById(R.id.quit_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.about_button:
                Intent about = new Intent(this, AboutMe.class);
                startActivity(about);
                break;
            case R.id.error_button:
                throw new RuntimeException("Generating an error");
            case R.id.sudoku_button:
                Intent sudoku = new Intent(this, Sudoku.class);
                startActivity(sudoku);
                break;
            case R.id.dictionary_button:
                Intent dictionary = new Intent(this, TestDictionary.class);
                startActivity(dictionary);
                break;
            case R.id.word_game_button:
                Intent bananagramsMenu = new Intent(this, BananagramsMenu.class);
                startActivity(bananagramsMenu);
                break;
            case R.id.communication_button:
                Intent commMenu = new Intent(this, CommunicationsMenu.class);
                startActivity(commMenu);
                break;
            case R.id.two_player_word_game_button:
                Intent twoPlayerWordGame = new Intent(this, MultiplayerMenu.class);
                startActivity(twoPlayerWordGame);
                break;
            case R.id.trickiest_part_button:
                Intent photoTest = new Intent(this, PictureTest.class);
                startActivity(photoTest);
                break;
            case R.id.commercial_cliche_button:
                Intent cc = new Intent(this, PlayStoreDescription.class);
                startActivity(cc);
                break;
            case R.id.quit_button:
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            default:
                assert(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
