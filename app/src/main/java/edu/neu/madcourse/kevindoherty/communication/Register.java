package edu.neu.madcourse.kevindoherty.communication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.io.IOException;

import edu.neu.madcourse.kevindoherty.InternetUtils;
import edu.neu.madcourse.kevindoherty.R;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.Endpoints;
import edu.neu.madcourse.kevindoherty.bananagrams.multiplayer.GcmUtils;

public class Register extends Activity implements View.OnClickListener {

    EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_register);
        GcmUtils.register(this);
        etName = (EditText) findViewById(R.id.user_name);
        findViewById(R.id.submit_button).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private SharedPreferences getPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void storeName(String name) {
        getPrefs().edit().putString("name", name).apply();
    }




    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.submit_button:

                Toast noNameError = Toast.makeText(this, "Please enter a name before submitting", Toast.LENGTH_SHORT);

                if (etName.getText() == null) {
                    noNameError.show();
                    break;
                }

                final String name = etName.getText().toString();

                if (Strings.isNullOrEmpty(name)) {
                    noNameError.show();
                    break;
                }

                if (name.length() > 10) {
                    Toast.makeText(this, "Please choose a username of 10 or less characters", Toast.LENGTH_SHORT).show();
                    break;
                }

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            String regId = GcmUtils.getRegistrationId(Register.this);
                            if (!Strings.isNullOrEmpty(regId)) {
                                if (InternetUtils.checkOnline(Register.this)) {
                                    Endpoints.REG.registerWithName(GcmUtils.getRegistrationId(Register.this), name).execute();
                                    Log.w("Register", "Registered");
                                    storeName(name);
                                }

                            } else {
                                Log.w("Register", "Did not register");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();

                Intent viewPlayers = new Intent(getApplicationContext(), RegisteredPlayers.class);
                startActivity(viewPlayers);
                break;
        }
    }
}
