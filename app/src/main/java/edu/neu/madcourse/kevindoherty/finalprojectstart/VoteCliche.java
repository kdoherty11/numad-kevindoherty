package edu.neu.madcourse.kevindoherty.finalprojectstart;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kdoherty on 11/25/14.
 */
public class VoteCliche extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "Vote Cliche";

    private long clicheId;
    private String userId;
    private VoteType type;

    enum VoteType { UP, DOWN }

    public static Set<Long> votedOn = new HashSet<>();

    public VoteCliche(long clicheId, String userId, VoteType type) {
        this.clicheId = clicheId;
        this.userId = userId;
        this.type = type;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try {
            if (type == VoteType.UP) {
                Endpoints.CLICHE_API.upVoteByClicheId(clicheId, userId).execute();
            } else if (type == VoteType.DOWN) {
                Endpoints.CLICHE_API.downVoteByClicheId(clicheId, userId).execute();
            }
        } catch (IOException e) {
            Log.e(TAG, "Problem voting cliche " + clicheId + " " + e.getMessage());
        }
        return null;
    }
}
