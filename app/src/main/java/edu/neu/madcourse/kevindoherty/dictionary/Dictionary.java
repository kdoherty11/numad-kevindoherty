package edu.neu.madcourse.kevindoherty.dictionary;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created by kdoherty on 9/24/14.
 */
public class Dictionary {

    private static final int ALPHABET_SIZE = 26;
    private static final int MAX_WORDS_PER_LETTER = 46822;
    private static final int INIT = 1;
    private static final String LARGE_WORDS_PATH = "largewords.txt";

    private long[][] mWordLists = new long[ALPHABET_SIZE][MAX_WORDS_PER_LETTER];
    // Keeps track of which characters are initialized.
    // Index 26 keeps track of large words initialization status.
    private AtomicIntegerArray mIsInit = new AtomicIntegerArray(ALPHABET_SIZE + 1);
    private Context mContext;
    private List<String> mLongWordList;

    public Dictionary(Context context) {
        mContext = context;
        initAsync();
    }

    private void initAsync() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        ExecutorService executor = Executors.newFixedThreadPool(ALPHABET_SIZE + 1);
        for (int i = 0; i < ALPHABET_SIZE; i++) {
            char current = alphabet.charAt(i);
            Runnable worker = new WordReaderAndEncoder(current);
            executor.execute(worker);
        }
        executor.execute(new LongWordReader());
        executor.shutdown();
    }

    private class WordReaderAndEncoder implements Runnable {

        private char c;

        private WordReaderAndEncoder(char c) {
            this.c = c;
        }

        @Override
        public void run() {
            initShortDict(c);
            mIsInit.set(getIndexFromChar(c), INIT);
        }
    }

    private class LongWordReader implements Runnable {
        @Override
        public void run() {
            mLongWordList = getWords(LARGE_WORDS_PATH);
            mIsInit.set(26, INIT);
        }
    }

    private void initShortDict(char c) {
        List<String> lines = getWords("wordlist_" + c + ".txt");
        long[] array = getWordArray(c);
        int index = 0;
        for (String word : lines) {
            index++;
            array[index] = encodeStringSmall(word);
        }
        Arrays.sort(array);
    }

    private List<String> getWords(String path) {
        final List<String> words = new ArrayList<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(mContext.getAssets().open(path)));
            String line;
            while ((line = br.readLine()) != null) {
                words.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return words;
    }

    private long encodeStringSmall(String string) {
        long encoded = 0l;
        int length = string.length();
        for (int i = 0; i < length; i++) {
            long m = string.codePointAt(i) - 96; // 'a' is represented at 97 so now it is represented as 1
            m <<= ((11 - i) * 5) + 4;
            encoded |= m;
        }
        return encoded;
    }

    private long[] getWordArray(char c) {
        return mWordLists[getIndexFromChar(c)];
    }

    private int getIndexFromChar(char c) {
        return Character.getNumericValue(c) - 10;
    }

    private boolean isInit(char c) {
        return mIsInit.get(getIndexFromChar(c)) == INIT;
    }

    public boolean isWord(String word) {
        String trimmedWord = word.toLowerCase().trim();
        int len = trimmedWord.length();
        if (len <= 12) {
            return isShortWord(trimmedWord);
        } else {
            return isLongWord(trimmedWord);
        }
    }

    private boolean isShortWord(String word) {
        if (word.length() == 0) {
            return false;
        }
        char first = word.charAt(0);
        while (!isInit(first));
        return Arrays.binarySearch(getWordArray(first), encodeStringSmall(word)) >= 0;
    }

    private boolean isLongWord(String word) {
        while (mIsInit.get(26) != INIT);
        return Collections.binarySearch(mLongWordList, word) >= 0;
    }
}
