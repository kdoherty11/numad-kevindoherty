package edu.neu.madcourse.kevindoherty.bananagrams;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import edu.neu.madcourse.kevindoherty.bananagrams.model.Tile;

/**
 * Created by kdoherty on 10/2/14.
 */
public class TileView extends FrameLayout {


    /** The row this is located on */
    private int row;

    /** The column this is located on */
    private int col;

    private Tile tile;

    private int holderIndex;

    public TileView(Context context) {
        super(context);
    }

    public TileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TileView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    void setRow(int row) {
        this.row = row;
    }
    void setCol(int col) {
        this.col = col;
    }
    void setTile(Tile tile) { this.tile = tile; }
    void setHolderIndex(int holderIndex) { this.holderIndex = holderIndex; }

    int getRow() {
        return row;
    }
    int getCol() {
        return col;
    }
    Tile getTile() { return tile; }
    int getHolderIndex() { return holderIndex; };

}