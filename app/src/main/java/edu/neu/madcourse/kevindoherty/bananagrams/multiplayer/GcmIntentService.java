package edu.neu.madcourse.kevindoherty.bananagrams.multiplayer;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.util.logging.Level;
import java.util.logging.Logger;

import edu.neu.madcourse.kevindoherty.R;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    private String senderRegId;
    private String gameId;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Intent Service";

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Intent received " + intent);
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);


        if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
            // Since we're not using two way messaging, this is all we really to check for
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Logger.getLogger("GCM_RECEIVED").log(Level.INFO, extras.toString());

                if (extras.containsKey("senderRegId")) {
                    senderRegId = extras.getString("senderRegId");
                } else {
                    Log.w(TAG, "Sender reg id not found in message");
                }

                if (extras.containsKey("gameId")) {
                    gameId = extras.getString("gameId");
                    Log.i(TAG, "gameId WAS found in message");
                } else {
                    Log.w(TAG, "gameId not found in message");
                }

                if (extras.containsKey("gameInviteResponse")) {
                    sendGameInviteResponse(extras.getString("message"), Boolean.valueOf(extras.getString("gameInviteResponse")));
                } else if (extras.containsKey("intent") && extras.getString("intent").equals("none")) {
                    if (extras.containsKey("notification") && extras.getString("notification").equals("none")) {
                        // TODO:
                    } else {
                        sendNotificationNoIntent(extras.getString("message"));
                    }
                } else {
                    sendNotification(extras.getString("message"));
                }

            } else {
                Log.d(TAG, "Message type: " + messageType);
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.banana_nobg)
                        .setContentTitle("Bananagrams")
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        Intent gameInvite = new Intent(this, GameInvite.class);
        if (Strings.isNullOrEmpty(senderRegId)) {
            Log.w(TAG, "Putting extra null or empty reg id");
        } else {
            Log.i(TAG, "Extra is legit");
        }
        gameInvite.putExtra("senderRegId", senderRegId);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                gameInvite, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotificationNoIntent(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.banana_nobg)
                        .setContentTitle("Bananagrams")
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendGameInviteResponse(String msg, boolean accept) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.banana_nobg)
                        .setContentTitle("Bananagrams")
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        if (accept) {
            Intent game = new Intent(this, TwoPlayer.class);
            game.putExtra("oppRegId", senderRegId);
            game.putExtra("gameId", gameId);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    game, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(contentIntent);
        }

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}

